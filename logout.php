<?php

session_start();

  require_once 'fbConfig_front.php';

// Remove access token from session
unset($_SESSION['facebook_access_token']);

// Remove user data from session
unset($_SESSION['userData']);

session_unset();
session_destroy();
ob_start();
header("location:https://book2wheel.com/asia/index.php");
ob_end_flush(); 
//include 'index.php';

exit();
?>
