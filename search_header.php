<?php  
session_start();    
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>book2wheel</title>

    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/jquery.min.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
          <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
   <!--   <link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.min.css" /> -->
     <link rel="stylesheet" href="css/bootstrap-datepicker.css" />

    <script type="text/javascript" src="js/moment.js"></script>
    <!-- <script type="text/javascript" src="js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <style type="text/css">
    	/* Split the screen in half */
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}

/* Control the left side */
.left {
  left: 0;
  background-color: #FFF;
}

/* Control the right side */
.right {
  right: 0;
 /* background-color: #FFF;*/
}

/* If you want the content centered horizontally and vertically */
.centered {
  width: 100%;
  height: 100%;
  margin:10px;
 /* position: absolute;*/
 /* top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;*/
}

/* Style the image inside the centered container, if needed */
.centered img {
  /*width: 150px;
  border-radius: 50%;*/
}
    </style>

    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      @media screen and (max-width: 600px) {
  .right{
    visibility: hidden;
    clear: both;
    float: left;
    margin: 10px auto 5px 20px;
    width: 28%;
    display: none;
  }
  .left{
      width: 100%;
  }
}
.shadow {
  border-bottom: 0 none;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.46);       

}
.imgBorder {
  padding: 15px 15px 0;
  background-color: white;
  /* border: 15px solid transparent;*/
    
}
    </style>
</head>
<body>
 <!--Main Navigation-->
<header>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top"  style="background-color: #00b5db;margin-bottom: 20px;float: right">
    <a class="navbar-brand" href="index.php">Book2wheel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse " id="navbarSupportedContent">       
     <?php

     if(isset($accessToken)){
    ?>
<ul class="navbar-nav mr-auto" style="float: right">
            <!-- <li class="nav-item active">
                <a class="nav-link" href="3">Inicio <span class="sr-only">(current)</span></a>
            </li> -->
             <a href="user_profile/index.php">Profile</a>
                        
            <li class="nav-item">
                <a class="nav-link" href="user_profile/addBike.php"><i class="fa fa-facebook-f"></i> Add bike</a>
            </li>
           <!--  <li class="nav-item">
                <a class="nav-link" href="https://goo.gl/e4LTRN"><i class="fa fa-twitter"></i>List Bikes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://goo.gl/M34Fcp"><i class="fa fa-instagram"></i>Rent Bikes</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="user_profile/index.php"><i classs="fa fa-pinterest"></i>Go to Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php"><i class="fa fa-tumblr"></i>Logout</a>
            </li>
        </ul>
         <!-- <ul class="navbar-nav ml-auto nav-flex-icons"> -->
           <!--  <li class="nav-item">
                <a class="nav-link waves-effect waves-light">14 <i class="fa fa-envelope"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">8 <i class="fa fa-eye"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">1 <i class="fa fa-bullhorn"></i></a>
            </li> -->
          <!--   <li class="nav-item avatar dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="http://www.torneodj.com/files/avatar/2_120.jpg" class="rounded z-depth-4" width="35" height="30"></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-purple" aria-labelledby="navbarDropdownMenuLink-5">
                    <a class="dropdown-item whov" href="#"><i class="fa fa-plus"></i> Agregar post</a>
                    <a class="dropdown-item whov" href="#"><i class="fa fa-edit"></i> Editar cuenta</a>
                </div>
            </li> -->
      <!--   </ul> -->

        <?php  } else if(!isset($_SESSION["firstname"]))
    { ?>
   <ul class="navbar-nav mr-auto"  style="float: right">
            <!-- <li class="nav-item active">
                <a class="nav-link" href="3">Inicio <span class="sr-only">(current)</span></a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="login/index.php"><i class="fa fa-facebook-f"></i> Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="login/index.php"><i class="fa fa-twitter">Sign Up</i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-instagram"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-pinterest"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-tumblr"></i></a>
            </li>
        </ul>
 
    <?php }   else { ?>

<ul class="navbar-nav mr-auto"  style="float: right">
            <!-- <li class="nav-item active">
                <a class="nav-link" href="3">Inicio <span class="sr-only">(current)</span></a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="https://goo.gl/DNTqG9"><i class="fa fa-facebook-f"></i> Add bike</a>
            </li>
           <!--  <li class="nav-item">
                <a class="nav-link" href="https://goo.gl/e4LTRN"><i class="fa fa-twitter"></i>List Bikes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://goo.gl/M34Fcp"><i class="fa fa-instagram"></i>Rent Bikes</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="user_profile/index.php"><i class="fa fa-pinterest"></i>Go to Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php"><i class="fa fa-tumblr"></i>Logout</a>
            </li>
        </ul>
         <!-- <ul class="navbar-nav ml-auto nav-flex-icons"> -->
           <!--  <li class="nav-item">
                <a class="nav-link waves-effect waves-light">14 <i class="fa fa-envelope"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">8 <i class="fa fa-eye"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect waves-light">1 <i class="fa fa-bullhorn"></i></a>
            </li> -->
          <!--   <li class="nav-item avatar dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="http://www.torneodj.com/files/avatar/2_120.jpg" class="rounded z-depth-4" width="35" height="30"></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-purple" aria-labelledby="navbarDropdownMenuLink-5">
                    <a class="dropdown-item whov" href="#"><i class="fa fa-plus"></i> Agregar post</a>
                    <a class="dropdown-item whov" href="#"><i class="fa fa-edit"></i> Editar cuenta</a>
                </div>
            </li> -->
     <!--    </ul> -->
    <?php } ?>
        
    </div>
</nav>
</header>