<?php

    
if(!session_id()){
    session_start();
}
 include('includes/allFunctions.php'); 

    require_once 'fbConfig_front.php';
    require_once 'User.class.php';
    
    if(isset($accessToken)){
    	
    	 // $code =  $_REQUEST['code'];
    	 // $_SESSION['code_num']= $_REQUEST['code'];

    if(isset($_SESSION['facebook_access_token'])){
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }else{
        // Put short-lived access token in session
        $_SESSION['facebook_access_token'] = (string) $accessToken;
        
          // OAuth 2.0 client handler helps to manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Exchanges a short-lived access token for a long-lived one
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
        
        // Set default access token to be used in script
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }
    
    // Redirect the user back to the same page if url has "code" parameter in query string
    // if(isset($_GET['code'])){
    //     header('Location: ./');
    // }
    
    // Getting user facebook profile info
    try {
        $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,cover,picture');
        $fbUserProfile = $profileRequest->getGraphNode()->asArray();
    } catch(FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        session_destroy();
        // Redirect user back to app login page
        header("Location: ./");
        exit;
    } catch(FacebookSDKException $e) {
        echo 'Facebook SDK returned in index: ' . $e->getMessage();
        exit;
    }
    
    // Initialize User class
    $user = new User();
    
    // Insert or update user data to the database
    $fbUserData = array(
        'oauth_provider'=> 'facebook',
        'oauth_uid'     => $fbUserProfile['id'],
        'first_name'    => $fbUserProfile['first_name'],
        'last_name'     => $fbUserProfile['last_name'],
        'email'         => $fbUserProfile['email'],
        'gender'        => $fbUserProfile['gender'],
        'locale'        => $fbUserProfile['locale'],
        'cover'         => $fbUserProfile['cover']['source'],
        'picture'       => $fbUserProfile['picture']['url'],
        'link'          => $fbUserProfile['link']
    );
    $userData = $user->checkUser($fbUserData);
    
    // Put user data into session
    $_SESSION['userData'] = $userData;
    $_SESSION['oauth_uid_fb']=$userData['oauth_uid'];
     
    facebook_session($userData['oauth_uid']);
  //  Get logout url
   $logoutURL = $helper->getLogoutUrl($accessToken, $redirectURL.'logout.php');
    
  //  Render facebook profile data
    if(!empty($userData)){
        $output  = '<h2 style="color:#999999;">Facebook Profile Details</h2>';
        $output .= '<div style="position: relative;">';
        $output .= '<img src="'.$userData['cover'].'" />';
        $output .= '<img style="position: absolute; top: 90%; left: 25%;" src="'.$userData['picture'].'"/>';
        $output .= '</div>';
        $output .= '<br/>Facebook ID : '.$userData['oauth_uid'];
        $output .= '<br/>Name : '.$userData['first_name'].' '.$userData['last_name'];
        $output .= '<br/>Email : '.$userData['email'];
        $output .= '<br/>Gender : '.$userData['gender'];
        $output .= '<br/>Locale : '.$userData['locale'];
        $output .= '<br/>Logged in with : Facebook';
        $output .= '<br/>Profile Link : <a href="'.$userData['link'].'" target="_blank">Click to visit Facebook page</a>';
        $output .= '<br/>Logout from <a href="'.$logoutURL.'">Facebook</a>'; 
    }else{
        $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
    }
    
}else{
    // Get login url
    // $loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);
    
    // // Render facebook login button
    // $output = '<a href="'.htmlspecialchars($loginURL).'"><img src="images/fblogin-btn.png"></a>';
}
   

// if(isset($_GET['code'])){
	

//     $code =  $_REQUEST['code'];
//     $_SESSION['code_num']= $_REQUEST['code'];
//             //header('Location: ./');
//          // header("Location: ../process_booking.php?code=".$code);   // die();


//     }



?>

<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--import Bootstrap 4 start -->
		
		<script src="js/jquery-1.9.1.js" type="text/javascript"></script>
		<script src="js/jquery.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui.js" type="text/javascript"></script>

       <link  href="css/datepicker.css" rel="stylesheet">



		<!--import Bootstrap end -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
		 <link href="css/bootstrap-social.css" rel="stylesheet">
		 <link href="css/font-awesome.min.css" rel="stylesheet">
          <link href="css/font-awesome.min.css" rel="stylesheet">

		<!-- Import font-awesome for soceal media icons -->
		<script defer src="Fonts/all.js"></script>
		<!-- Import google material icons -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> <!-- used for the Place-icon -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
		<title>Book2Wheel</title>
    
      <!-- ... -->

      <!--  <link href="css/bootstrap.css" rel="stylesheet"> -->
      <link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	  <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
   <!--   <link rel="stylesheet" href="css/bootstrap-datetimepicker-standalone.min.css" /> -->
     <link rel="stylesheet" href="css/bootstrap-datepicker.css" />

	  <script type="text/javascript" src="js/moment.js"></script>
	  <!-- <script type="text/javascript" src="js/bootstrap.min.js"></script> -->
	  <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	  

<!-- 
     <link href="assets/css/bootstrap.min.css" rel="stylesheet">
   
    <link href="assets/css/bootstrap-datepicker.css" rel="stylesheet">
     <script src="assets/js//jquery.min.js"></script> -->
    
    
     

<!-- 
		<script src="assets/scripts/jquery.min.js"></script>
       <script src="assets/scripts/jquery-1.9.1.js" type="text/javascript"></script>
       <script src="assets/scripts/jquery-ui.js" type="text/javascript"></script> -->

        <link href="css/responsive.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <style> 
@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css););
</style>
		<style>
			@font-face {
			  font-family: 'Ludicrous';
			  src: url('Fonts/Ludicrous-Regular.eot');
			  src: url('Fonts/Ludicrous-Regular.woff2') format('woff2'),
			       url('Fonts/Ludicrous-Regular.woff') format('woff'),
			       url('Fonts/Ludicrous-Regular.ttf') format('truetype'),
			       url('Fonts/Ludicrous-Regular.svg#Ludicrous-Regular') format('svg'),
			       url('Fonts/Ludicrous-Regular.eot?#iefix') format('embedded-opentype');
			  font-weight: normal;
			  font-style: normal;
			}
			font-family: 'Open Sans', sans-serif;
			body {
		    background-color: white;
		    overflow-y:hidden;
			}, html {
			    height: 100%;
			    margin: 0;
			}
			h1 {
			    color: white;
			    font-family: Ludicrous;
			    font-size: 90px;
			    font-weight: 700px;
			} 
			h2 {
				color: #0a4a87;
				font-family: Ludicrous;
			}
			p {
				color: #2d2d2d;
				font-family: Open Sans;
				text-align: center;
			}
			.header {
			  	background-image: linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3) ), url(header.jpg);
			  	width: 100%;
			  	min-height: 1000px;
			  	background-position: center;
			    background-repeat: no-repeat;
			    background-size: cover;
			}
			#header-logo {
				position: absolute;
				left: 50px;
				top: 40px;
				height: 64px;
				width: 110px;
			}
			.button-container {
				position: absolute;
				right: 50px;
				top: 46px;
			}
			.button {
				color: white;
				padding: 10px 20px 10px 20px;
				margin-right: 20px;
				background-color: rgba(0,0,0,0.5);
				font-weight: 400;
				border-radius: 10px;
				margin-top: 20px;
			}
			#sign-up {
				background-color: #eb4d34;
			}
			.openbtn {
				position: absolute;
			    top: 25px;
			    right: 25px;
			    font-size:24px;
			    cursor:pointer;
			    color: white;
			}
			.search-city {
				padding-top: 100px;
				align-content: center;
				text-align: center;
				padding-right: 60px; 
				padding-left: 60px;
			}
			#search-city-input {
				max-width: 860px;
				margin: auto;
				padding-bottom: 48px;
			}
			.pheader {
				font-family: Open Sans;
				color: white;
				padding-top: 10px;
				font-size: 18px;
				font-weight: 400;
			}
			#lets-ride {
				background-color: #eb4d34;
				padding: 10px 40px 10px 40px;
				margin: auto;
				font-size: 18px;
				font-weight: 600;
				width:auto;
			}
			.title {
				padding-top: 100px;
				padding-bottom: 60px;
			    color: #0a4a87;				
				text-align: center;
				font-family: Ludicrous;
				font-size: 60px;
			}
			.icon {
			    margin-top: 8px;
				width: 150px;
				height: 150px;
			    display: block;
			    margin-left: auto;
			    margin-right: auto;			
			}
			.iconcontainer {
				width: 250px;
			    display: block;
			    margin-left: auto;
			    margin-right: auto;
			    text-align: center;
			    padding-bottom: 20px;
			}
			.ptitle {
				padding-top: 36px;
				font-weight: bold;
			}
			p.justify {
			    text-align: justify;
			}
			.testimonial-container {
				width: 1000px;
			    margin-left: auto;
			    margin-right: auto;
			    display: table;
   			    margin-bottom: 116px;
			}
			.profile-img {
				max-height: 164px;
				border-radius: 50%;
				margin: 60px 25px 60px 70px;
			}
			.testimonial-name-container {
				padding-right: 20px;
				display: table-cell;
				vertical-align: middle;
				white-space: nowrap; 
			}
			p.h4 {
				font-family: Open Sans;
				font-size: 16px;
				font-weight: 700;
				text-align: left;
				word-break: keep-all;
			}
			p.h5 {
				font-family: Open Sans;
				font-size: 14px;
				font-weight: 400;
				text-align: left;
				word-break: keep-all;
			}
			.testimonial-text-container {
				padding-right: 20px;
				display: table-cell;
				vertical-align: middle;
			}
			p.h6 {
				font-family: Open Sans;
				font-size: 16px;
				font-weight: 300;
				text-align: left;
			}

			.footer-title {
				text-align: left;
				font-weight: bold;
				font-size: 24px;
				color: #383838;
			}
			.footer-title-mobile {
				text-align: center;
				font-weight: bold;
				font-size: 24px;
				color: #383838;
				padding-top: 55px;
				padding-bottom: 0px;

			}
			.footer-link {
				text-align: left;
				font-size: 18px;
				color: #3b3b3b;
			}
			#footer-logo {
				height: 86px;
				width: 140px;
				display: block;
			    margin-left: auto;
			    margin-right: auto;
			    text-align: center;
			}
			.footer {
				background-color: #f1f1f1;
				padding: 100px 0px 50px 30px;
				line-height:2;
			}
			.footer-mobile {
				background-color: #f1f1f1;
				padding-top: 44px;
			    margin-left: auto;
			    margin-right: auto;
			    text-align: center;
			    line-height:2;
			}
			.footer-mobile-cell {

			}
			.footer-social {
				margin-left: auto;
			    margin-right: auto;
			    text-align: center;
			    color: #02497f;
			    padding-top: 40px;
			    font-size: 24px;
			    letter-spacing:24px;
			}
			a {
				margin-top: 8px;
			}
			a:link {
			    color: #3b3b3b;
			}
			a:visited {
			    color: #3b3b3b;
			}
			a:hover {
			    color: #3b3b3b;
			}
			a:active {
			    color: #3b3b3b;
			}
			@media screen and (max-width: 992px) {
			  div.hide-mobile {
			    display: none;
			}
		</style>

		<style type="text/css">
		     

input[type=text], input[type=password] {
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}
select {
	   width: 40%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}
.error{
	color : red;
}
#numPhone{
	 width: 59%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}
#day{
	 width: 30%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}
#month{
	 width: 30%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}
#year{
	 width: 30%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
}

hr {
    border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
}

button:hover {
    opacity:1;
}

/* Extra styles for the cancel button */
.cancelbtn {
    padding: 14px 20px;
    background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
  float: left;
  width: 50%;
}

/* Add padding to container elements */
.container {
    padding: 16px;
}

/* Clear floats */
.clearfix::after {
    content: "";
    clear: both;
    display: table;
}


.dropbtn {
   /* background-color: #4CAF50;*/
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 100px;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shad;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #ddd}

.dropdown:hover .dropdown-content {
    display: block;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shad;


}

.dropdown:hover .dropbtn {
   /* background-color: #3e8e41;*/
}

		</style>

		<script type="text/javascript">
           $(document).ready(function () {
          
  $(".navbar-nav li a").click(function(event) {
    $(".navbar-collapse").collapse('hide');
  });
});
        </script>
        <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5af180c4227d3d7edc250e4e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
	</head>

		<body>
		<div class="header">


			<img id="header-logo" src="icons/Book2WheelLogo.png">
           
              <div class="col-12 col-md-12">
                    <div class="menu_area">

                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                           
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse in" id="ca-navbar">

                            	<?php
                                    if (!$accessToken && !@$_SESSION['firstname'] ){
                            	?>
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <li class="nav-item btn button active" ><a class="nav-link  " href="index.php" style="color: #FFF" >Home</a></li>
                                    <li class="nav-item btn button " ><a style="color: #FFF"  class="nav-link " href="#" data-toggle="modal" data-target="#accEmail">Add Bike</a></li>
                                    <li class="nav-item btn button" ><a style="color: #FFF" class="nav-link"  href="http://www.book2wheel.com/blog/frequently-asked-questions/" target="_blank">Help</a></li>
                                <!--     <li class="nav-item btn button" ><a style="color: #FFF" class="nav-link " href="#">DKK</a></li> -->
                                    <li class="nav-item btn button" style="background-color: #eb4d34"><a class="nav-link  " style="color: #FFF" data-toggle="modal" data-target="#accEmail">Create Profil</a></li>
                                    <li class="nav-item btn button" style="background-color: #eb4d34"><a style="color: #FFF" class="nav-link" href="#"  data-toggle="modal" data-target="#loginAcc">Login</a></li>
                                     <!-- <li class="nav-item btn button active" ><a class="nav-link  " href="logout.php" style="color: #FFF" >logout</a></li> -->
                                   <!--  <li class="nav-item"><a class="nav-link" href="#team">Team</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li> -->
                                </ul>

                                <?php  } else {
                                    
                                 ?>
                                 <ul class="navbar-nav ml-auto" id="nav">
                                    <li class="nav-item btn button active" ><a class="nav-link  " href="index.php" style="color: #FFF" >Home</a></li><!-- 
                                    <li class="nav-item btn button " ><a style="color: #FFF"  class="nav-link " href="user_profile/add_Bike.php">Add</a></li> -->
                                    <li class="nav-item btn button" ><a style="color: #FFF" class="nav-link"  href="user_profile/addBike.php?oauth_uid=<?php  echo $userData['oauth_uid']; ?>">Add your Bike</a></li>
                                <!--     <li class="nav-item btn button" ><a style="color: #FFF" class="nav-link " href="#">DKK</a></li> -->
                                  <!--   <li class="nav-item><a class="nav-link " href="user_profile/index.php" style="color: #FFF;">Profil</a></li>
 -->
                                     <li ><div class="dropdown">
                                     	  <?php 
                                     	        if ($_SESSION['firstname']) { 
                                                       $data = get_img_desc($_SESSION['id']);
                                                        foreach((array)$data as $datas ) {
                                                        	$image = $datas['image'];
                                                        	$img_loc="";
                                                        	if ($image == ""){
                                                             $img_loc = "user_profile/uploads/default.jpg";
                                                        	}else{
                                                        	  $img_loc = "user_profile/uploads/".$image;
                                                        	}
                                     	        	?>
                                                 <img class="dropbtn" width="100px" height="100px" style="border-radius: 50%;" src="<?php  echo $img_loc;  ?>"  />
                                     	     <?php } ?>

                                                  <div class="dropdown-content">
											    <a href="user_profile/index.php">Profile</a>
											    <a href="user_profile/listing_bikes.php">Listing Bike</a>
											    <a href="user_profile/profile_account.php">settings</a>
											    <a href="logout.php">logout</a>
											  </div>
                                     	      <?php } 
                                     	      elseif($accessToken) { ?>
                                                <img class="dropbtn" style="border-radius: 50%;" src="<?php  echo $userData['picture'];  ?>" />
                                                 <div class="dropdown-content">
											    <a href="user_profile/index.php?oauth_uid=<?php  echo $userData['oauth_uid']; ?>">Profile</a>
											    <a href="user_profile/listing_bikes.php?oauth_uid=<?php  echo $userData['oauth_uid']; ?>">Listing Bike</a>
											    <a href="user_profile/profile_account.php?oauth_uid=<?php  echo $userData['oauth_uid']; ?>">settings</a>
											    <a href="logout.php">logout</a>
											  </div>
                                     	     <?php }
                                     	               	  ?>
                              
											 
                                        </div>
                                     </li> 
                                  <!--   <li class="nav-item btn button" style="background-color: #eb4d34"><a style="color: #FFF" class="nav-link" href="logout.php"  >Logout</a></li> -->
                                     <!-- <li class="nav-item btn button active" ><a class="nav-link  " href="logout.php" style="color: #FFF" >logout</a></li> -->
                                   <!--  <li class="nav-item"><a class="nav-link" href="#team">Team</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li> -->
                                </ul>
                                
                             <?php   } 
                                ?>
                               <!--  <div class="sing-up-button d-lg-none">
                                    <a href="#">Login</a>
                                </div>
                                <div class="sing-up-button d-lg-none">
                                    <a href="#">Sign-Up</a>
                                </div>
                                -->
                                
                            </div>
                        </nav>
                    </div>
                </div>
			
			<!-- <span class="openbtn d-xl-none d-lg-none">&#9776;</span> -->
			
			<div class="search-city">
				<h1>Motorbike sharing service</h1>
				<p class="pheader">Search by city name. Free booking. Pay on pickup.</p>
				<div id="search-city-input">
					 <form role="form"  id="search_b" method="GET" action="bikesearch.php">
             <input type="hidden" name="opera" value="searchBike">
					<input type="text" class="form-control input-lg validate" placeholder="Type in location, where you want to rent" id="autocomplete" name="autocomplete" placeholder="Enter your address"  onFocus="geolocate()" required="required">	
					 <input class="date-own form-control input-lg validate" style="width: 200px;" type="text" required="required" id="date1" name="date1" placeholder="Start Date">

					  <input data-toggle="datepicker" class="date-own form-control input validate" style="width: 200px;" type="text" required="required" id="date2" name="date2" placeholder="Return Date">			
				</div>
				<button type="submit" id="lets-ride" class="btn button" id="search_bikes">Let's ride!</button>
			</form>
			</div>

		</div>

