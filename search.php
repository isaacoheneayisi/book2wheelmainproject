<?php
//require("phpsqlajax_dbinfo.php");
include ('includes/config.php');

function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}



// // Opens a connection to a MySQL server
// $connection=mysqli_connect ('localhost', 'root', '');
// if (!$connection) {
//   die('Not connected : ' . mysqli_error());
// }

// // Set the active MySQL database
// $db_selected = mysqli_select_db('book2wheel', $connection);
// if (!$db_selected) {
//   die ('Can\'t use db : ' . mysqli_error());
// }

// // Select all the rows in the markers table
// $query = "SELECT * FROM bikes WHERE 1";
// $result = mysqli_query($query);
// if (!$result) {
//   die('Invalid query: ' . mysqli_error());
// }
$address = $_GET['autocomplete'];
 $str = explode(',', $address);
    $strr="";
    $ex_address="";
    if ($str){
          $ex_address = $str[0]; 
    } else{
        $strr= explode(' ', $address);
        $ex_address = $strr[0]; 
    }

global $mysqli;
        $sql = "SELECT id,make,autocomplete ,daily FROM bikes WHERE autocomplete like '%$ex_address%' AND active=1 ";
       $result = $mysqli->query($sql);
      if (!$result) {
      die('Invalid query: ' . mysqli_error());
         }
  //  while ($row = $result->fetch_array()) {
  // $lat = getLat($row['autocomplete']);
  // $lon= getLon($row['autocomplete']);
  //  echo $lon ."".$lat;
  //  }


header("Content-type: text/xml");

// Start XML file, echo parent node
echo "<?xml version='1.0' ?>";
echo '<markers>';

$type ="restaurant";
$ind=0;
// Iterate through the rows, printing XML nodes for each
while ($row = $result->fetch_array()){
  $lat = getLat($row['autocomplete']);
  $lon= getLon($row['autocomplete']);
  // Add to XML document node
  echo '<marker ';
  echo 'id="' . $row['id'] . '" ';
  echo 'name="' . parseToXML($row['daily']) . '" ';
  echo 'address="' . parseToXML($row['autocomplete']) . '" ';  
  echo 'lat="' . $lat . '" ';
  echo 'lng="' . $lon. '" ';
  echo 'type="' . $type. '" ';
  echo '/>';
  $ind = $ind + 1;
}

// End XML file
echo '</markers>';

function getLat($address){

 $url = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&address=".urlencode($address);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response_a = json_decode($response);
$lat = $response_a->results[0]->geometry->location->lat;
return $lat;
}
function getLon($address){
//$address = "Cafe Coffee Day, INSIDE HPCL Petrol Bunk, Mumbai - Goa Highway,    Mangaon, Maharashtra, India";
 $url = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&address=".urlencode($address);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response_a = json_decode($response);
 $long = $response_a->results[0]->geometry->location->lng;
 return $long;
}

?>