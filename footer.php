
		<div class="row footer hide-mobile">
			<div class="col-3">
				<img id="footer-logo" src="icons/Book2WheelLogo.png">
				<div class="footer-social">
					<a class="fab fa-instagram" href="#"></a>
					<a class="fab fa-facebook" href="#"></a>						
					<a class="fab fa-linkedin-in" href="#"></a>	
				</div>
			</div>
			<div class="col-3">
				<p class="footer-title">Quick links</p>
				<a href="#">Blog</a><br>
				<a href="#">How it works</a><br>
				<a href="#">Motorbike insurance</a><br>
				<a href="#">Motorbike tours</a><br>
			</div>
			<div class="col-3">
				<p class="footer-title">Book2Wheel</p>
				<a href="#">Our story</a><br>
				<a href="#">Become partner</a><br>
				<a href="#">Add motorbike</a><br>
				<a href="#">Contact</a><br>
				<a href="#">Terms &amp; Conditions</a><br>
			</div>
			<div class="col-3">
				<p class="footer-title">Help</p>
				<a href="#">Support</a><br>
				<a href="#">FAQ</a><br>
				<a href="#">Contact</a><br>
			</div>
		</div>
<!-- footer menu on mobile-->
		<div class="footer-mobile d-xl-none d-lg-none">
			<div class="">
				<img id="footer-logo" src="icons/Book2WheelLogo.png">
				<div class="footer-social">
					<a class="fab fa-instagram" href="#"></a>
					<a class="fab fa-facebook" href="#"></a>						
					<a class="fab fa-linkedin-in" href="#"></a>	
				</div>
			</div>
			<div class="footer-mobile-cell">
				<p class="footer-title-mobile">Quick links</p>
				<a href="#">Blog</a><br>
				<a href="#">How it works</a><br>
				<a href="#">Motorbike insurance</a><br>
				<a href="#">Motorbike tours</a><br>
			</div>
			<div class="footer-mobile-cell">
				<p class="footer-title-mobile">Book2Wheel</p>
				<a href="#">Our story</a><br>
				<a href="#">Become partner</a><br>
				<a href="#">Add motorbike</a><br>
				<a href="#">Contact</a><br>
				<a href="#">Terms &amp; Conditions</a><br>
			</div>
			<div class="footer-mobile-cell">
				<p class="footer-title-mobile">Help</p>
				<a href="#">Support</a><br>
				<a href="#">FAQ</a><br>
				<a href="#">Contact</a><br>
			</div>
		</div>
    
	<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!--  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<!--   <script src="user_profile/js/bootstrap.min.js"></script> -->
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
 <script src="assets/scripts/jquery.blockui.min.js" type="text/javascript"></script>

    <!-- <script src="assets/toastr/toastr.min.js" type="text/javascript"></script>   -->
     <script src="assets/scripts/jquery.validate.min.js" type="text/javascript"></script>
     <script src="assets/scripts/jquery.validate.js" type="text/javascript"></script>
 <script src="user_profile/js/bootstrap-datepicker.js"></script>
  <script src="user_profile/js/loadingoverlay.min.js"></script> 
    <script type="text/javascript">
     // dt = new Date();
      // var d = new Date();
      // var n = d.getFullYear();
     // var year = date.getYear();
       
$('#date1').datepicker({
  autoclose:true,
  startDate:'-0d'
}).on('changeDate',function(e){
  $('#date2').datepicker('setStartDate',e.date)
});

$('#date2').datepicker({
  autoclose:true
});



// function updateToDate(day,month,year){     
//    var myDate = new Date();
//     var updatedDay;
//     if(month==9|| month==4 ||month==6 || month==11){
//         updatedDay = 30;
//     }
//     else if (month==2){
//         updatedDay = 28;
//     }
//     else{
//         updatedDay = 31;
//     }
//    var prettyDate =month+ '/' +updatedDay + '/' +year;
// $("#date2").val(prettyDate);
// }

// $(function() {
//     $( "#date2" ).datepicker({  
//          // startDate: '-0d'  
//         //minDate: dateToday
//     });
// });
//       // $('#date1').datepicker({
      //   format: 'mm/dd/yyyy',
      //   startDate: '-0d'
      //  });

      //  // var year = date.getYear();
      // $('#date2').datepicker({
      //    var dropdt = new Date(document.getElementById("date1").value);
      //   format: 'mm/dd/yyyy',
      //   startDate: '-0'+dropdt
      //  });
       $('#date3').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-0d'
       });
        $('#date4').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-0d'
       });
  </script>


<?php if (@$page_title == "index") :?>
   <script src="assets/scripts/create_user.js" type="text/javascript"></script>
	 <script>
    // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        //street_number: 'short_name',
        //route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
    //   autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
        <script src="https://maps.googleapis.com/maps/api/js?**v=3;**&key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&libraries=places&callback=initAutocomplete" async defer></script>
         <script type="text/javascript" src="js/bikesearch.js"></script>
    <?php endif; ?>

    <?php if (@$page_title == "bikesearch") :?>

    	 <script>
    // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        //street_number: 'short_name',
        //route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('searchAdd')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
    //   autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
        <script src="https://maps.googleapis.com/maps/api/js?**v=3;**&key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&libraries=places&callback=initAutocomplete" async defer></script>

    <?php endif; ?>

  

	</body>
</html>

