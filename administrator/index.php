<?php
session_start();

// if(isset($_GET['code'])){
//     $code =  $_REQUEST['code'];
//     $_SESSION['code_num']= $_REQUEST['code'];
//         //header('Location: ./');
//          header("Location: ../process_booking.php?code=".$code);
//          die();
//     }

// Include FB config file && User class
// require_once '../fbConfig.php';
// require_once '../User.class.php';

// if(isset($accessToken)){
//     if(isset($_SESSION['facebook_access_token'])){
//         $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
//     }else{
//         // Put short-lived access token in session
//         $_SESSION['facebook_access_token'] = (string) $accessToken;
        
//           // OAuth 2.0 client handler helps to manage access tokens
//         $oAuth2Client = $fb->getOAuth2Client();
        
//         // Exchanges a short-lived access token for a long-lived one
//         $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
//         $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
        
//         // Set default access token to be used in script
//         $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
//     }
    
//     // Redirect the user back to the same page if url has "code" parameter in query string
//     if(isset($_GET['code'])){
//         header('Location: ./');
//     }
    
//     // Getting user facebook profile info
//     try {
//         $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,cover,picture');
//         $fbUserProfile = $profileRequest->getGraphNode()->asArray();
//     } catch(FacebookResponseException $e) {
//         echo 'Graph returned an error: ' . $e->getMessage();
//         session_destroy();
//         // Redirect user back to app login page
//         header("Location: ./");
//         exit;
//     } catch(FacebookSDKException $e) {
//         echo 'Facebook SDK returned an error: ' . $e->getMessage();
//         exit;
//     }
    
//     // Initialize User class
//     $user = new User();
    
//     // Insert or update user data to the database
//     $fbUserData = array(
//         'oauth_provider'=> 'facebook',
//         'oauth_uid'     => $fbUserProfile['id'],
//         'first_name'    => $fbUserProfile['first_name'],
//         'last_name'     => $fbUserProfile['last_name'],
//         'email'         => $fbUserProfile['email'],
//         'gender'        => $fbUserProfile['gender'],
//         'locale'        => $fbUserProfile['locale'],
//         'cover'         => $fbUserProfile['cover']['source'],
//         'picture'       => $fbUserProfile['picture']['url'],
//         'link'          => $fbUserProfile['link']
//     );
//     $userData = $user->checkUser($fbUserData);
    
//     // Put user data into session
//     $_SESSION['userData'] = $userData;
    
//     // Get logout url
//     $logoutURL = $helper->getLogoutUrl($accessToken, $redirectURL.'logout.php');
    
//     // Render facebook profile data
//     if(!empty($userData)){
//         $output  = '<h2 style="color:#999999;">Facebook Profile Details</h2>';
//         $output .= '<div style="position: relative;">';
//         $output .= '<img src="'.$userData['cover'].'" />';
//         $output .= '<img style="position: absolute; top: 90%; left: 25%;" src="'.$userData['picture'].'"/>';
//         $output .= '</div>';
//         $output .= '<br/>Facebook ID : '.$userData['oauth_uid'];
//         $output .= '<br/>Name : '.$userData['first_name'].' '.$userData['last_name'];
//         $output .= '<br/>Email : '.$userData['email'];
//         $output .= '<br/>Gender : '.$userData['gender'];
//         $output .= '<br/>Locale : '.$userData['locale'];
//         $output .= '<br/>Logged in with : Facebook';
//         $output .= '<br/>Profile Link : <a href="'.$userData['link'].'" target="_blank">Click to visit Facebook page</a>';
//         $output .= '<br/>Logout from <a href="'.$logoutURL.'">Facebook</a>'; 
//     }else{
//         $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
//     }
    
// }else{
//     // Get login url
//     $loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);
  //  2
//     // Render facebook login button
//     $output = '<a href="'.htmlspecialchars($loginURL).'"><img src="images/fblogin-btn.png"></a>';
// }
//    // $date1 = $_POST['datepicker'];
// //session_start();
if(!isset($_SESSION["firstname"]) )
    { ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
     <style type="text/css">
         .error{
            color: red;
         }
     </style>
     <script>

</script>
    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                	
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Admin</strong> Login </h1>
                            <div class="description">
                            	<!-- <p>
	                            	This is a free responsive <strong>"login and register forms"</strong> template made with Bootstrap. 
	                            	Download it on <a href="http://azmind.com" target="_blank"><strong>AZMIND</strong></a>, 
	                            	customize and use it as you like!
                            	</p> -->
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-10" >
                        	
                        	<div class="form-box" >
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>LOGIN</h3>
	                            		<!-- <p>Enter username and password to log on:</p> -->
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form"  method="post" id="adminForm">
                                             <input type="hidden" name="opera" value="adminRequest">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-username">Username</label>
				                        	<input type="text" name="userName" id="userName" placeholder="Enter UserName..." class="form-username form-control" >
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="passwd" id="passwd" placeholder="Password..." class="form-password form-control">
				                        </div>
				                        <button type="submit" class="btn">Sign in!</button>
				                    </form>
			                    </div>
		                    </div>
		                
		                	<!-- <div class="social-login">
	                        	<h3>...or login with:</h3>
	                        	<div class="social-login-buttons">
		                        	<a class="btn btn-link-1 btn-link-1-facebook" href="https://www.facebook.com/dialog/oauth?client_id=248521672555954&redirect_uri=https://book2wheel.com/admintest/login/index.php&scope=email">
		                        		<i class="fa fa-facebook"></i> Facebook
		                        	</a>

                                    <a href="https://www.facebook.com/v2.10/dialog/oauth?client_id=374376776049353&amp;state=bae1ab0937ee56cdfb420377f87c5a8c&amp;response_type=code&amp;sdk=php-sdk-5.6.2&amp;redirect_uri=http%3A%2F%2Fdemos.codexworld.com%2Flogin-with-facebook-using-php%2F&amp;scope=email"><img src="images/fblogin-btn.png"></a> -->
		                        <!-- 	<a class="btn btn-link-1 btn-link-1-twitter" href="#">
		                        		<i class="fa fa-twitter"></i> Twitter
		                        	</a>
		                        	<a class="btn btn-link-1 btn-link-1-google-plus" href="">
		                        		<i class="fa fa-google-plus"></i> Google Plus
		                        	</a>
	                        	</div>
	                        </div> -->
	                        
                        </div>
                        
                       <!--  <div class="col-sm-1 middle-border"></div>
                        <div class="col-sm-1"></div> -->
                      
                    </div>
                    
                </div>
            </div>
            
        </div>

        <?php  } 

        else { 

            header("Location: ../process_booking.php");

            die(); ?>
            <div><?php echo $output; ?></div>

           <?php ?>

                <h3>proceed with check out</h3>

        <?php

         }

        ?>

        <!-- Footer -->
 <!--        <footer>
        	<div class="container">
        		<div class="row">
        			
        			<div class="col-sm-8 col-sm-offset-2">
        				<div class="footer-border"></div>
        				<p>Made by Anli Zaimi at <a href="http://azmind.com" target="_blank"><strong>AZMIND</strong></a> 
        					having a lot of fun. <i class="fa fa-smile-o"></i></p>
        			</div>
        			
        		</div>
        	</div>
        </footer> -->

        <!-- Javascript -->


 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
       


  <script src="../user_profile/js/bootstrap.min.js"></script>
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 -->    
<!--  <script src="assets/scripts/jquery.blockui.min.js" type="text/javascript"></script> -->

    <!-- <script src="assets/toastr/toastr.min.js" type="text/javascript"></script>   -->
     <script src="../assets/scripts/jquery.validate.min.js" type="text/javascript"></script>
     <script src="../assets/scripts/jquery.validate.js" type="text/javascript"></script>
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
        <script src="../user_profile/js/loadingoverlay.min.js"></script> 
 <script src="../assets/scripts/book_user.js" type="text/javascript"></script>
    </body>

</html>