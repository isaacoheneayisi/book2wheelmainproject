<?php 
    

   include('header.php') ; 
     $page_title ="index";
    
 ?>

<script type="text/javascript">
	  $(document).ready(function () {
           	$("#forgotForm").hide();
           });
</script>
<style type="text/css">
body {
	font-family: "Open Sans", sans-serif;
}
h2 {
	color: #333;
	text-align: center;
	text-transform: uppercase;
	font-family: "Roboto", sans-serif;
	font-weight: bold;
	position: relative;
	margin: 25px 0 50px;
}
h2::after {
	content: "";
	width: 100px;
	position: absolute;
	margin: 0 auto;
	height: 3px;
	background: #ffdc12;
	left: 0;
	right: 0;
	bottom: -10px;
}
.carousel {
	width: 650px;
	margin: 0 auto;
	padding-bottom: 50px;
}
.carousel .item {
	color: #ffd700;
	font-size: 14px;
    text-align: center;
	overflow: hidden;
    min-height: 340px;
}
.carousel .item a {
	color: #ffd700;
}
.carousel .img-box {
	width: 145px;
	height: 145px;
	margin: 0 auto;
	border-radius: 50%;
}
.carousel .img-box img {
	width: 100%;
	height: 100%;
	display: block;
	border-radius: 50%;
}
.carousel .testimonial {	
	padding: 30px 0 10px;
}
.carousel .overview {	
	text-align: center;
	padding-bottom: 5px;
}
.carousel .overview b {
	color: #333;
	font-size: 15px;
	text-transform: uppercase;
	display: block;	
	padding-bottom: 5px;
}
.carousel .star-rating i {
	font-size: 18px;
	color: #ffdc12;
}
.carousel .carousel-control {
	width: 30px;
	height: 30px;
	border-radius: 50%;
    background: #999;
    text-shadow: none;
	top: 4px;
}
.carousel-control i {
	font-size: 20px;
	margin-right: 2px;
}
.carousel-control.left {
	left: auto;
	right: 40px;
}
.carousel-control.right i {
	margin-right: -2px;
}
.carousel .carousel-indicators {
	bottom: 15px;
}
.carousel-indicators li, .carousel-indicators li.active {
	width: 11px;
	height: 11px;
	margin: 1px 5px;
	border-radius: 50%;
}
.carousel-indicators li {	
	background: #e2e2e2;
	border-color: transparent;
}
.carousel-indicators li.active {
	border: none;
	background: #888;		
}
</style>
<!-- <div class="search-city">
			</div>
			<div class="search-city">
				<h1>Motorbike sharing service</h1>
				<p class="pheader">Search by city name. Free booking. Pay on pickup.</p>
				<div id="search-city-input">
					<input type="text" class="form-control input-lg" placeholder="Type in location, where you want to rent" name="city">
					<span class="material-icons" style="color:#a2a2a2">place</span> -->
				<!-- </div>
				<button type="button" id="lets-ride" class="btn button">Let's ride!</button>
			</div>
 -->
		<!-- </div> --> 


		<div class="title">How it works</div>
		<div class="row">
			<div class="col-lg-4 col-12">
				<div class="iconcontainer">
					<img class="icon" src="icons/search.svg">
					<p class="ptitle">Make your search</p>
					<p>Type in a location, pick your dates and type of two wheels. Click Let's ride.</p>
				</div>
			</div>
			<div class="col-lg-4 col-12">
				<div class="iconcontainer">
					<img class="icon" src="icons/funnel.svg">
					<p class="ptitle">Find a bike you like</p>
					<p>Use the filters on the page to find the perfect bike to ride then click on the bike you like.</p>
				</div>
			</div>
			<div class="col-lg-4 col-12">
				<div class="iconcontainer">
					<img class="icon" src="icons/like.svg">
					<p class="ptitle">Make your booking</p>
					<p>Confirm your booking and you will be provided a pick-up location - that's it.</p>
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8">
				<div class="title">What is Book2Wheel?</div>
				<p class="justify">Book2Wheel is an online business that helps you find the motorbike/bike you need, or allow you to share yours with others. It helps establishing contacts between individuals - bike owners and bike sharing and rental within one platform.</p>
				<p class="justify">The vision with Book2Wheel is promoting the sharing economy by allowing for a win - win situation for the bike owner and bike rentee. Furthermore assisting the process with security and ensurance help.</p>
			</div>
		</div>
		<div class="col-2"></div>
		<div class="title">Why Book2Wheel?</div>
		<div class="row">
			<div class="col-lg-3 col-12">
				<div class="iconcontainer">
					<img class="icon" src="icons/money.svg">
					<p class="ptitle">Compare prices</p>
					<p>Choose the best offers on motorbike rental.</p>
				</div>
			</div>
			<div class="col-lg-3 col-12">
				<div class="iconcontainer">
					<img class="icon" src="icons/tag.svg">
					<p class="ptitle">Special deals</p>
					<p>We negotiate with owners and get you discount on long term rentals.</p>
				</div>
			</div>
			<div class="col-lg-3 col-12">
				<div class="iconcontainer">
					<img class="icon" src="icons/user.svg">
					<p class="ptitle">Safety</p>
					<p>We provide comprehensive insurance for motorbike owners.</p>
				</div>
			</div>
			<div class="col-lg-3 col-12">
				<div class="iconcontainer">
					<img class="icon" src="icons/chat.svg">
					<p class="ptitle">Improve live</p>
					<p>We create opportunities for everyone to save time and money through the power of connectivity.</p>
				</div>
			</div>
		</div>

<!-- The Carousel has been disabled/outcommented because it does not funktion properly. Needs fixing.

<div id="carouselTestimonials" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
 -->
<div class="hide-mobile">
	<h2 class="title">Testimonials</h2>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Carousel indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>   
	<!-- Wrapper for carousel items -->
	<div class="carousel-inner">		
		<div class="item carousel-item active">
			<div class="img-box"><a href="https://www.facebook.com/book2wheel/reviews/"  target="_blank" alt="view fb reviews"><img src="img/testimonials/mathew.jpg" alt=""><a/></div>
			<p class="testimonial">I've rented from this company twice and each time they've been exceptional. For a fee of 200 pesos they will drop off the scooter at your hotel. Pick up is also 200 pesos which is great when you're not staying in Makati. Super friendly. Great new scooters that run really well. Easy contracts. I highly recommend the company. I'll use them again when I'm back, for sure.</p>
			<p class="overview"><b>Matthew Rock</b> <a href="#">.</a></p>
			<div class="star-rating">
				<ul class="list-inline">
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
				</ul>
			</div>
		</div>
		<div class="item carousel-item">
			<div class="img-box"><a href="https://www.facebook.com/book2wheel/reviews/" target="_blank" alt="view fb reviews"><img src="img/testimonials/Leyden.jpg" alt=""></a></div>
			<p class="testimonial">Thank you Book2Wheel for helping us, it was really fast response and you have very good customer service to find good bike for us. Renting motorbike here in Manila is really a wise idea if you don't want to be stuck in traffic. Will surely ask for your service again! Thank you!</p>
			<p class="overview"><b>Leyden Du</b><a href="#"></a></p>
			<div class="star-rating">
				<ul class="list-inline">
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
				</ul>
			</div>
		</div>
		<div class="item carousel-item">
			<div class="img-box"><a href="https://www.facebook.com/book2wheel/reviews/" target="_blank" alt="view fb reviews"><img src="img/testimonials/Bogdan.jpg"" alt=""></a></div>
			<p class="testimonial">Seldom do you find companies and people that would go an extra mile for you. Book2Wheel not only managed our last-minute late evening response, but made sure that it went well. As a first visit to Philippines, it's nice to know that we found an online rental service that can be trusted. Next time we need a bike, Book2Wheel is our go-to place. And is highly recommended for you too!
P.S. I also personally liked the no-bullshit Viber communication approach. Almost always online and ready to help. Thanks a ton! Prosperous future development to you, guys!.</p>
			<p class="overview"><b>Bogdan Ponomarov</b> <a href="#">.</a></p>
			<div class="star-rating">
				<ul class="list-inline">
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
				</ul>
			</div>
		</div>	
		<div class="item carousel-item">
			<div class="img-box"><a href="https://www.facebook.com/book2wheel/reviews/" target="_blank" alt="view fb reviews"><img src="img/testimonials/Josh.jpg"" alt=""></a></div>
			<p class="testimonial">I was going for my first visit to the Philippines and Book2wheel really helped me to locate and rent a motorcycle in Cebu City. They gave me all the necessary information of the rental place and also made follow up inquiries to make sure everything turned out ok. Great customer service. I will definitely use Book2wheel again!.</p>
			<p class="overview"><b>Josh Donnelly</b> <a href="#">.</a></p>
			<div class="star-rating">
				<ul class="list-inline">
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
				</ul>
			</div>
		</div>
			<div class="item carousel-item">
			<div class="img-box"><a href="https://www.facebook.com/book2wheel/reviews/" target="_blank" alt="view fb reviews"><img src="img/testimonials/John.jpg"" alt=""></a></div>
			<p class="testimonial">These guys are awesome to deal with, they will even drop off the motorbike to your hotel and pick up .
Cheap , and 100% trusted.</p>
			<p class="overview"><b>John Houben</b> <a href="#">.</a></p>
			<div class="star-rating">
				<ul class="list-inline">
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star"></i></li>
					<li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
				</ul>
			</div>
		</div>	
	</div>
	<!-- Carousel controls -->
	<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
		<i class="fa fa-angle-left"></i>
	</a>
	<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
		<i class="fa fa-angle-right"></i>
	</a>
</div>
    
</div>

<!-- <a href="https://www.facebook.com/v2.10/dialog/oauth?client_id=374376776049353&amp;state=80fbac20af25bfb8c7e5b45627ede829&amp;response_type=code&amp;sdk=php-sdk-5.6.2&amp;redirect_uri=http%3A%2F%2Fdemos.codexworld.com%2Flogin-with-facebook-using-php%2F&amp;scope=email"><img src="images/fblogin-btn.png"></a>
 -->
<div class="modal fade" id="accEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
              <b style="float: right"> Create a free Profile </b>
            </div>

   <form id="regform" style="border:1px solid #ccc"  method="post">
  <div class="container">
   
    <p style=" margin: auto;">Register with one click </p><br />
    
    <p style=" margin: auto;"><a href="https://www.facebook.com/dialog/oauth?client_id=815037232021791&state=e635d9f9ef98662893ea9911443939f0&response_type=code&sdk=php-sdk-5.6.2&redirect_uri=https://book2wheel.com/asia/index.php&scope=email" ><img src="img/FB.jpg" width="150px" height="50px"></a>  <!-- <a href=""><img src="img/google.png"  height="50px" width="50px"></a> -->    </p>

    <hr>

    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" id="email" required>

    <label for="FirstName"><b>FirstName</b></label>
    <input type="text" placeholder="Enter FirstName" name="firstname" id="firstname" required>
   
   <label for="FirstName"><b>LasName</b></label>
    <input type="text" placeholder="Enter LastName" name="lastname"  id="lastname" required>


    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" id="psw" required>

    <label for="psw_repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="pswrepeat"  id="pswrepeat" required>

    <label for="phone_number"><b>Phone Number</b></label><br />
    
      <select name="countryCode" id="countryCode">
      	<option data-countryCode="DK" value="45" Selected>Denmark (+45)</option>
	<option data-countryCode="GB" value="44" >UK (+44)</option>
	<option data-countryCode="US" value="1">USA (+1)</option>
	<optgroup label="Other countries">
		<option data-countryCode="DZ" value="213">Algeria (+213)</option>
		<option data-countryCode="AD" value="376">Andorra (+376)</option>
		<option data-countryCode="AO" value="244">Angola (+244)</option>
		<option data-countryCode="AI" value="1264">Anguilla (+1264)</option>
		<option data-countryCode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
		<option data-countryCode="AR" value="54">Argentina (+54)</option>
		<option data-countryCode="AM" value="374">Armenia (+374)</option>
		<option data-countryCode="AW" value="297">Aruba (+297)</option>
		<option data-countryCode="AU" value="61">Australia (+61)</option>
		<option data-countryCode="AT" value="43">Austria (+43)</option>
		<option data-countryCode="AZ" value="994">Azerbaijan (+994)</option>
		<option data-countryCode="BS" value="1242">Bahamas (+1242)</option>
		<option data-countryCode="BH" value="973">Bahrain (+973)</option>
		<option data-countryCode="BD" value="880">Bangladesh (+880)</option>
		<option data-countryCode="BB" value="1246">Barbados (+1246)</option>
		<option data-countryCode="BY" value="375">Belarus (+375)</option>
		<option data-countryCode="BE" value="32">Belgium (+32)</option>
		<option data-countryCode="BZ" value="501">Belize (+501)</option>
		<option data-countryCode="BJ" value="229">Benin (+229)</option>
		<option data-countryCode="BM" value="1441">Bermuda (+1441)</option>
		<option data-countryCode="BT" value="975">Bhutan (+975)</option>
		<option data-countryCode="BO" value="591">Bolivia (+591)</option>
		<option data-countryCode="BA" value="387">Bosnia Herzegovina (+387)</option>
		<option data-countryCode="BW" value="267">Botswana (+267)</option>
		<option data-countryCode="BR" value="55">Brazil (+55)</option>
		<option data-countryCode="BN" value="673">Brunei (+673)</option>
		<option data-countryCode="BG" value="359">Bulgaria (+359)</option>
		<option data-countryCode="BF" value="226">Burkina Faso (+226)</option>
		<option data-countryCode="BI" value="257">Burundi (+257)</option>
		<option data-countryCode="KH" value="855">Cambodia (+855)</option>
		<option data-countryCode="CM" value="237">Cameroon (+237)</option>
		<option data-countryCode="CA" value="1">Canada (+1)</option>
		<option data-countryCode="CV" value="238">Cape Verde Islands (+238)</option>
		<option data-countryCode="KY" value="1345">Cayman Islands (+1345)</option>
		<option data-countryCode="CF" value="236">Central African Republic (+236)</option>
		<option data-countryCode="CL" value="56">Chile (+56)</option>
		<option data-countryCode="CN" value="86">China (+86)</option>
		<option data-countryCode="CO" value="57">Colombia (+57)</option>
		<option data-countryCode="KM" value="269">Comoros (+269)</option>
		<option data-countryCode="CG" value="242">Congo (+242)</option>
		<option data-countryCode="CK" value="682">Cook Islands (+682)</option>
		<option data-countryCode="CR" value="506">Costa Rica (+506)</option>
		<option data-countryCode="HR" value="385">Croatia (+385)</option>
		<option data-countryCode="CU" value="53">Cuba (+53)</option>
		<option data-countryCode="CY" value="90392">Cyprus North (+90392)</option>
		<option data-countryCode="CY" value="357">Cyprus South (+357)</option>
		<option data-countryCode="CZ" value="42">Czech Republic (+42)</option>		
		<option data-countryCode="DJ" value="253">Djibouti (+253)</option>
		<option data-countryCode="DM" value="1809">Dominica (+1809)</option>
		<option data-countryCode="DO" value="1809">Dominican Republic (+1809)</option>
		<option data-countryCode="EC" value="593">Ecuador (+593)</option>
		<option data-countryCode="EG" value="20">Egypt (+20)</option>
		<option data-countryCode="SV" value="503">El Salvador (+503)</option>
		<option data-countryCode="GQ" value="240">Equatorial Guinea (+240)</option>
		<option data-countryCode="ER" value="291">Eritrea (+291)</option>
		<option data-countryCode="EE" value="372">Estonia (+372)</option>
		<option data-countryCode="ET" value="251">Ethiopia (+251)</option>
		<option data-countryCode="FK" value="500">Falkland Islands (+500)</option>
		<option data-countryCode="FO" value="298">Faroe Islands (+298)</option>
		<option data-countryCode="FJ" value="679">Fiji (+679)</option>
		<option data-countryCode="FI" value="358">Finland (+358)</option>
		<option data-countryCode="FR" value="33">France (+33)</option>
		<option data-countryCode="GF" value="594">French Guiana (+594)</option>
		<option data-countryCode="PF" value="689">French Polynesia (+689)</option>
		<option data-countryCode="GA" value="241">Gabon (+241)</option>
		<option data-countryCode="GM" value="220">Gambia (+220)</option>
		<option data-countryCode="GE" value="7880">Georgia (+7880)</option>
		<option data-countryCode="DE" value="49">Germany (+49)</option>
		<option data-countryCode="GH" value="233">Ghana (+233)</option>
		<option data-countryCode="GI" value="350">Gibraltar (+350)</option>
		<option data-countryCode="GR" value="30">Greece (+30)</option>
		<option data-countryCode="GL" value="299">Greenland (+299)</option>
		<option data-countryCode="GD" value="1473">Grenada (+1473)</option>
		<option data-countryCode="GP" value="590">Guadeloupe (+590)</option>
		<option data-countryCode="GU" value="671">Guam (+671)</option>
		<option data-countryCode="GT" value="502">Guatemala (+502)</option>
		<option data-countryCode="GN" value="224">Guinea (+224)</option>
		<option data-countryCode="GW" value="245">Guinea - Bissau (+245)</option>
		<option data-countryCode="GY" value="592">Guyana (+592)</option>
		<option data-countryCode="HT" value="509">Haiti (+509)</option>
		<option data-countryCode="HN" value="504">Honduras (+504)</option>
		<option data-countryCode="HK" value="852">Hong Kong (+852)</option>
		<option data-countryCode="HU" value="36">Hungary (+36)</option>
		<option data-countryCode="IS" value="354">Iceland (+354)</option>
		<option data-countryCode="IN" value="91">India (+91)</option>
		<option data-countryCode="ID" value="62">Indonesia (+62)</option>
		<option data-countryCode="IR" value="98">Iran (+98)</option>
		<option data-countryCode="IQ" value="964">Iraq (+964)</option>
		<option data-countryCode="IE" value="353">Ireland (+353)</option>
		<option data-countryCode="IL" value="972">Israel (+972)</option>
		<option data-countryCode="IT" value="39">Italy (+39)</option>
		<option data-countryCode="JM" value="1876">Jamaica (+1876)</option>
		<option data-countryCode="JP" value="81">Japan (+81)</option>
		<option data-countryCode="JO" value="962">Jordan (+962)</option>
		<option data-countryCode="KZ" value="7">Kazakhstan (+7)</option>
		<option data-countryCode="KE" value="254">Kenya (+254)</option>
		<option data-countryCode="KI" value="686">Kiribati (+686)</option>
		<option data-countryCode="KP" value="850">Korea North (+850)</option>
		<option data-countryCode="KR" value="82">Korea South (+82)</option>
		<option data-countryCode="KW" value="965">Kuwait (+965)</option>
		<option data-countryCode="KG" value="996">Kyrgyzstan (+996)</option>
		<option data-countryCode="LA" value="856">Laos (+856)</option>
		<option data-countryCode="LV" value="371">Latvia (+371)</option>
		<option data-countryCode="LB" value="961">Lebanon (+961)</option>
		<option data-countryCode="LS" value="266">Lesotho (+266)</option>
		<option data-countryCode="LR" value="231">Liberia (+231)</option>
		<option data-countryCode="LY" value="218">Libya (+218)</option>
		<option data-countryCode="LI" value="417">Liechtenstein (+417)</option>
		<option data-countryCode="LT" value="370">Lithuania (+370)</option>
		<option data-countryCode="LU" value="352">Luxembourg (+352)</option>
		<option data-countryCode="MO" value="853">Macao (+853)</option>
		<option data-countryCode="MK" value="389">Macedonia (+389)</option>
		<option data-countryCode="MG" value="261">Madagascar (+261)</option>
		<option data-countryCode="MW" value="265">Malawi (+265)</option>
		<option data-countryCode="MY" value="60">Malaysia (+60)</option>
		<option data-countryCode="MV" value="960">Maldives (+960)</option>
		<option data-countryCode="ML" value="223">Mali (+223)</option>
		<option data-countryCode="MT" value="356">Malta (+356)</option>
		<option data-countryCode="MH" value="692">Marshall Islands (+692)</option>
		<option data-countryCode="MQ" value="596">Martinique (+596)</option>
		<option data-countryCode="MR" value="222">Mauritania (+222)</option>
		<option data-countryCode="YT" value="269">Mayotte (+269)</option>
		<option data-countryCode="MX" value="52">Mexico (+52)</option>
		<option data-countryCode="FM" value="691">Micronesia (+691)</option>
		<option data-countryCode="MD" value="373">Moldova (+373)</option>
		<option data-countryCode="MC" value="377">Monaco (+377)</option>
		<option data-countryCode="MN" value="976">Mongolia (+976)</option>
		<option data-countryCode="MS" value="1664">Montserrat (+1664)</option>
		<option data-countryCode="MA" value="212">Morocco (+212)</option>
		<option data-countryCode="MZ" value="258">Mozambique (+258)</option>
		<option data-countryCode="MN" value="95">Myanmar (+95)</option>
		<option data-countryCode="NA" value="264">Namibia (+264)</option>
		<option data-countryCode="NR" value="674">Nauru (+674)</option>
		<option data-countryCode="NP" value="977">Nepal (+977)</option>
		<option data-countryCode="NL" value="31">Netherlands (+31)</option>
		<option data-countryCode="NC" value="687">New Caledonia (+687)</option>
		<option data-countryCode="NZ" value="64">New Zealand (+64)</option>
		<option data-countryCode="NI" value="505">Nicaragua (+505)</option>
		<option data-countryCode="NE" value="227">Niger (+227)</option>
		<option data-countryCode="NG" value="234">Nigeria (+234)</option>
		<option data-countryCode="NU" value="683">Niue (+683)</option>
		<option data-countryCode="NF" value="672">Norfolk Islands (+672)</option>
		<option data-countryCode="NP" value="670">Northern Marianas (+670)</option>
		<option data-countryCode="NO" value="47">Norway (+47)</option>
		<option data-countryCode="OM" value="968">Oman (+968)</option>
		<option data-countryCode="PW" value="680">Palau (+680)</option>
		<option data-countryCode="PA" value="507">Panama (+507)</option>
		<option data-countryCode="PG" value="675">Papua New Guinea (+675)</option>
		<option data-countryCode="PY" value="595">Paraguay (+595)</option>
		<option data-countryCode="PE" value="51">Peru (+51)</option>
		<option data-countryCode="PH" value="63">Philippines (+63)</option>
		<option data-countryCode="PL" value="48">Poland (+48)</option>
		<option data-countryCode="PT" value="351">Portugal (+351)</option>
		<option data-countryCode="PR" value="1787">Puerto Rico (+1787)</option>
		<option data-countryCode="QA" value="974">Qatar (+974)</option>
		<option data-countryCode="RE" value="262">Reunion (+262)</option>
		<option data-countryCode="RO" value="40">Romania (+40)</option>
		<option data-countryCode="RU" value="7">Russia (+7)</option>
		<option data-countryCode="RW" value="250">Rwanda (+250)</option>
		<option data-countryCode="SM" value="378">San Marino (+378)</option>
		<option data-countryCode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
		<option data-countryCode="SA" value="966">Saudi Arabia (+966)</option>
		<option data-countryCode="SN" value="221">Senegal (+221)</option>
		<option data-countryCode="CS" value="381">Serbia (+381)</option>
		<option data-countryCode="SC" value="248">Seychelles (+248)</option>
		<option data-countryCode="SL" value="232">Sierra Leone (+232)</option>
		<option data-countryCode="SG" value="65">Singapore (+65)</option>
		<option data-countryCode="SK" value="421">Slovak Republic (+421)</option>
		<option data-countryCode="SI" value="386">Slovenia (+386)</option>
		<option data-countryCode="SB" value="677">Solomon Islands (+677)</option>
		<option data-countryCode="SO" value="252">Somalia (+252)</option>
		<option data-countryCode="ZA" value="27">South Africa (+27)</option>
		<option data-countryCode="ES" value="34">Spain (+34)</option>
		<option data-countryCode="LK" value="94">Sri Lanka (+94)</option>
		<option data-countryCode="SH" value="290">St. Helena (+290)</option>
		<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>
		<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>
		<option data-countryCode="SD" value="249">Sudan (+249)</option>
		<option data-countryCode="SR" value="597">Suriname (+597)</option>
		<option data-countryCode="SZ" value="268">Swaziland (+268)</option>
		<option data-countryCode="SE" value="46">Sweden (+46)</option>
		<option data-countryCode="CH" value="41">Switzerland (+41)</option>
		<option data-countryCode="SI" value="963">Syria (+963)</option>
		<option data-countryCode="TW" value="886">Taiwan (+886)</option>
		<option data-countryCode="TJ" value="7">Tajikstan (+7)</option>
		<option data-countryCode="TH" value="66">Thailand (+66)</option>
		<option data-countryCode="TG" value="228">Togo (+228)</option>
		<option data-countryCode="TO" value="676">Tonga (+676)</option>
		<option data-countryCode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
		<option data-countryCode="TN" value="216">Tunisia (+216)</option>
		<option data-countryCode="TR" value="90">Turkey (+90)</option>
		<option data-countryCode="TM" value="7">Turkmenistan (+7)</option>
		<option data-countryCode="TM" value="993">Turkmenistan (+993)</option>
		<option data-countryCode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
		<option data-countryCode="TV" value="688">Tuvalu (+688)</option>
		<option data-countryCode="UG" value="256">Uganda (+256)</option>
		<!-- <option data-countryCode="GB" value="44">UK (+44)</option> -->
		<option data-countryCode="UA" value="380">Ukraine (+380)</option>
		<option data-countryCode="AE" value="971">United Arab Emirates (+971)</option>
		<option data-countryCode="UY" value="598">Uruguay (+598)</option>
		<!-- <option data-countryCode="US" value="1">USA (+1)</option> -->
		<option data-countryCode="UZ" value="7">Uzbekistan (+7)</option>
		<option data-countryCode="VU" value="678">Vanuatu (+678)</option>
		<option data-countryCode="VA" value="379">Vatican City (+379)</option>
		<option data-countryCode="VE" value="58">Venezuela (+58)</option>
		<option data-countryCode="VN" value="84">Vietnam (+84)</option>
		<option data-countryCode="VG" value="84">Virgin Islands - British (+1284)</option>
		<option data-countryCode="VI" value="84">Virgin Islands - US (+1340)</option>
		<option data-countryCode="WF" value="681">Wallis &amp; Futuna (+681)</option>
		<option data-countryCode="YE" value="969">Yemen (North)(+969)</option>
		<option data-countryCode="YE" value="967">Yemen (South)(+967)</option>
		<option data-countryCode="ZM" value="260">Zambia (+260)</option>
		<option data-countryCode="ZW" value="263">Zimbabwe (+263)</option>
	</optgroup>
</select>
<input type="text" name="numPhone"  id="numPhone" placeholder="Enter a valid phone Number"  required>
<label>BirthDay</label>

 <label> 	
    <select name="day" id="day">
      	<option  value="1" Selected>Day</option>
      	<option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
      </select>
      <select name="month" id="month">
      	<option  value="1" Selected>Month</option>
      	<option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
      </select>
       <select name="year" id="year">
      	<option  value="1" Selected>Year</option>
      	<option value="2014">2014</option>
          <option value="2013">2013</option>
          <option value="2012">2012</option>
          <option value="2011">2011</option>
          <option value="2010">2010</option>
          <option value="2009">2009</option>
          <option value="2008">2008</option>
          <option value="2007">2007</option>
          <option value="2006">2006</option>
          <option value="2005">2005</option>
          <option value="2004">2004</option>
          <option value="2003">2003</option>
          <option value="2002">2002</option>
          <option value="2001">2001</option>
          <option value="2000">2000</option>
          <option value="1999">1999</option>
          <option value="1998">1998</option>
          <option value="1997">1997</option>
          <option value="1996">1996</option>
          <option value="1995">1995</option>
          <option value="1994">1994</option>
          <option value="1993">1993</option>
          <option value="1992">1992</option>
          <option value="1991">1991</option>
          <option value="1990">1990</option>
          <option value="1989">1989</option>
          <option value="1988">1988</option>
          <option value="1987">1987</option>
          <option value="1986">1986</option>
          <option value="1985">1985</option>
          <option value="1984">1984</option>
          <option value="1983">1983</option>
          <option value="1982">1982</option>
          <option value="1981">1981</option>
          <option value="1980">1980</option>
          <option value="1979">1979</option>
          <option value="1978">1978</option>
          <option value="1977">1977</option>
          <option value="1976">1976</option>
          <option value="1975">1975</option>
          <option value="1974">1974</option>
          <option value="1973">1973</option>
          <option value="1972">1972</option>
          <option value="1971">1971</option>
          <option value="1970">1970</option>
          <option value="1969">1969</option>
          <option value="1968">1968</option>
          <option value="1967">1967</option>
          <option value="1966">1966</option>
          <option value="1965">1965</option>
          <option value="1964">1964</option>
          <option value="1963">1963</option>
          <option value="1962">1962</option>
          <option value="1961">1961</option>
          <option value="1960">1960</option>
          <option value="1959">1959</option>
          <option value="1958">1958</option>
          <option value="1957">1957</option>
          <option value="1956">1956</option>
          <option value="1955">1955</option>
          <option value="1954">1954</option>
          <option value="1953">1953</option>
          <option value="1952">1952</option>
          <option value="1951">1951</option>
          <option value="1950">1950</option>
          <option value="1949">1949</option>
          <option value="1948">1948</option>
          <option value="1947">1947</option>
          <option value="1946">1946</option>
          <option value="1945">1945</option>
          <option value="1944">1944</option>
          <option value="1943">1943</option>
          <option value="1942">1942</option>
          <option value="1941">1941</option>
          <option value="1940">1940</option>
          <option value="1939">1939</option>
          <option value="1938">1938</option>
          <option value="1937">1937</option>
          <option value="1936">1936</option>
          <option value="1935">1935</option>
          <option value="1934">1934</option>
          <option value="1933">1933</option>
          <option value="1932">1932</option>
          <option value="1931">1931</option>
          <option value="1930">1930</option>
          <option value="1929">1929</option>
          <option value="1928">1928</option>
          <option value="1927">1927</option>
          <option value="1926">1926</option>
          <option value="1925">1925</option>
          <option value="1924">1924</option>
          <option value="1923">1923</option>
          <option value="1922">1922</option>
          <option value="1921">1921</option>
          <option value="1920">1920</option>
          <option value="1919">1919</option>
          <option value="1918">1918</option>
          <option value="1917">1917</option>
          <option value="1916">1916</option>
          <option value="1915">1915</option>
          <option value="1914">1914</option>
          <option value="1913">1913</option>
          <option value="1912">1912</option>
          <option value="1911">1911</option>
          <option value="1910">1910</option>
          <option value="1909">1909</option>
          <option value="1908">1908</option>
          <option value="1907">1907</option>
          <option value="1906">1906</option>
          <option value="1905">1905</option>
          <option value="1904">1904</option>
          <option value="1903">1903</option>
          <option value="1902">1902</option>
          <option value="1901">1901</option>
          <option value="1900">1900</option>
          
      </select>
    
    <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
 </label>


    <div class="clearfix">
      <button type="button" class="cancelbtn" data-dismiss="modal" >Cancel</button>
      <button type="submit" class="signupbtn" id="submit">Sign Up</button>
    </div>
  </div>
</form>
            
    </div>
</div>
</div>
	



<!-- The Carousel has been disabled/outcommented because it does not funktion properly. Needs fixing.

	<div class="testimonial-container carousel-item">
		<div>
			<img class="profile-img" src="Testimonials/2.jpg" alt="John Houben">    		
		</div>
		<div class="testimonial-name-container">
			<span><p class="h4">John Houben</p></span>
			<span><p class="h5">From: Facebook</p></span>
		</div>
		<div class="testimonial-text-container">
			<p class="h6">These guys are awesome to deal with, they will even drop off the motorbike to your hotel and pick up. Cheap, and 100% trusted.</p>
		</div>
	</div>
	<div class="testimonial-container carousel-item">
		<div>
			<img class="profile-img" src="Testimonials/3.jpg" alt="Rafael Ruiz">    		
		</div>
		<div class="testimonial-name-container">
			<span><p class="h4">Rafael Ruiz</p></span>
			<span><p class="h5">From: Facebook</p></span>
		</div>
		<div class="testimonial-text-container">
			<p class="h6">Had to ride down south of Cebu last minute and book2wheels came through for me. Had some difficulty with one of their affiliate rental companies because I was Filipino and they only rent out to foreigners which I found ridiculous. Book2wheels vouched for me but not without me leaving a deposit with the affiliate. Book in advance because the bike you want may not be available.</p>
		</div>
	</div>
	  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	  </a>
	</div>
</div>
 -->


<div class="modal fade" id="conf_form"  role="dialog"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
              <b style="float: right"> Please verify the information that has been sent to your email </b>
            </div>
  
   <form id="codeform" style="border:1px solid #ccc"  method="post">
   	 <input type="hidden" name="opera" value="confrimcode">
  <div class="container"> 
    
    <hr>

    <label for="confCode"><b>Enter Confirmation Code</b></label>
    <input type="text" placeholder="Enter Confirmation code X-X-X-X-X " name="confCode" id="confCode" required>

    <div class="clearfix">
      <button type="button" class="cancelbtn" data-dismiss="modal" >Cancel</button>
      <button type="submit" class="signupbtn" id="submit_code">Confirm Code</button>
    </div>
  </div>
</form>
            
    </div>
</div>
</div>

<div class="modal fade" id="loginAcc"  role="dialog" >
    <div class="modal-dialog" role="document" >
        <div class="modal-content" id="modcon_id">
           
  
   <form id="loginForm" style="border:1px solid #ccc"  method="post">
   	 <div class="modal-header text-center">
              <b style="margin: auto"> Login</b>
            </div>
   	 <input type="hidden" name="opera" value="loginRequest">
  <div class="container"> 
   
    <label for="confCode"><b>Email</label>
    <input type="text" placeholder="Enter your email " name="emaillog" id="emaillog" required>

    <label for="confCode"><b>Password</b></label>s
    <input type="password" placeholder="Enter password " name="passwd" id="passwd" required>
    
     <div class="clearfix">
      <button type="button" class="cancelbtn" data-dismiss="modal" >Cancel</button>
      <button type="submit" class="signupbtn" id="submit_log">Login</button>
    </div>
     <p style=" margin: auto;"><a  href="login/reset_pass.php" style="color: blue" href="" >Forgot your password?</a></p><br />
    <p style=" margin: auto;">Login with one click using  </p><br />
    
    <p style=" margin: auto;"><a  href="https://www.facebook.com/dialog/oauth?client_id=815037232021791&state=e635d9f9ef98662893ea9911443939f0&response_type=code&sdk=php-sdk-5.6.2&redirect_uri=https://book2wheel.com/asia/index.php&scope=email" ><img src="img/FB.jpg" width="150px" height="50px"></a>  <!-- <img src="img/google.png"  height="50px" width="50px"></a> -->    </p>

 <!--   <a href="https://www.facebook.com/v2.10/dialog/oauth?client_id=374376776049353&amp;state=4f66cf1ba072bf772ae471ca4997db7f&amp;response_type=code&amp;sdk=php-sdk-5.6.2&amp;redirect_uri=http%3A%2F%2Fdemos.codexworld.com%2Flogin-with-facebook-using-php%2F&amp;scope=email"><img src="images/fblogin-btn.png"></a> -->
  </div>
</form>

 
            
    </div>
</div>
</div>

<!-- 
<div class="modal fade" id="frgetPass"  role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
              <b style="margin: auto">Reset Password</b>
            </div>
  
   <form id="forgotForm" style="border:1px solid #ccc"  method="post">
   	 <input type="hidden" name="opera" value="forgotRequest">
  <div class="container"> 
   
    <label for="confCode"><b>Email</label>
    <input type="text" placeholder="Enter your email " name="for_email" id="for_email" required>

    <label for="confCode"><b>Password</b></label>
    <input type="pasword" placeholder="Enter password " name="passwd" id="passwd" required> -->
   <!--  
     <div class="clearfix">
      <button type="button" class="cancelbtn" data-dismiss="modal" >Cancel</button>
      <button type="submit" class="signupbtn" id="forgot_sub">Reset</button>
    </div>

   
  </div>
</form>
            
    </div>
</div>
</div> --> 
 
	
</script>

 <?php  include ('footer.php'); ?>
