<?php include('header.php') ; 
 $page_title ="Chat Messages";
?>
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
 
 <style type="text/css">
      .error {
        color : red
      }
 </style>


 <script>
  function chat_ajax(){
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
      if(req.readyState == 4 && req.status == 200){
        document.getElementById('chat').innerHTML = req.responseText;
      }
    }
    req.open('GET', 'messages.php', true);
    req.send(); 
  }
  
  setInterval(function(){chat_ajax()}, 1000)
</script>

<style type="text/css">
  *{
    padding: 0;
    margin: 0;
    border: 0;
}
 
body{
    background: silver;
}
 
#container{
    width: 100%;
    background: white;
    margin: 0 auto;
    padding: 20px;
}
 
#chat_box{
    width: 90%;
    height: 400px;
    overflow-y:auto;
}
 
#chat_data{
    width: 100%;
    padding: 5px;
    margin-bottom: 5px;
    border-bottom: 1px solid silver;
    font-weight: bold;
}
 
input[type="text"]{
    width: 100%;
    height: 40px;
    border: 1px solid grey;
    border-radius: 5px;
}
 
input[type="submit"]{
    width: 100%;
    height: 40px;
    border: 1px solid grey;
    border-radius: 5px;
}
 
textarea{
    width: 100%;
    height: 40px;
    border: 1px solid grey;
    border-radius: 5px;
}

</style>

 <div class="col-md-9">
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Messages</h3>
                </div>
                <div class="panel-body">
                  <!-- Steps form -->
<div class="card">
   <!-- <h2> Pending Bikes </h2>
   <hr> -->
    <div class="card-body mb-4">
       
       <!DOCTYPE html>
<html>
    <head>
        <title>My Chat App</title>
        <link rel="stylesheet" href="style.css" media="all" />
    </head>
    
    <body>
        <div id="container">
            <div id="chat_box">
               <?php 
                      $data = get_chat();
                        foreach ((array)$data as $chat) {
                     
                  ?>
                <div id="chat_data">
                 
                    <span style="color:green;"><?php  echo $chat['name'];  ?></span>
                    <span style="color:brown;"><?php  echo $chat['msg'];  ?></span>
                    <span style="float:right;"><?php  echo $chat['date'];  ?></span>
                </div>

                  <?php }  ?>

            </div>

            <?php  

                $id = getUser_Name_sessID ();
            ?>
            
            <form id="chat_form" method="post" action="messages.php">
              <div style="width: 90% ">
                     <input type="hidden" name="name" placeholder="Enter Name: " value="<?php echo $id; ?>" />
                <textarea name="enter_message" placeholder="Enter Message"></textarea>
                <input type="submit" name="submit" value="Send!" />
              </div>
            
            </form>
        </div>

        <?php
  if(isset($_POST['submit'])){
    global $mysqli;
    $name = $_POST['name'];
    $msg = $_POST['enter_message'];
 
    $query = "INSERT INTO chat (name,msg) VALUES ('$name','$msg')";
    $run  = $mysqli->query($query);
    //$run = $con->query($query);
  }  
?>






    </body>

<?php include('footer.php') ; ?>

 <!-- <script src="assets/toastr/toastr.min.js" type="text/javascript"></script>   -->
    <!--  <script src="../assets/scripts/jquery.validate.min.js" type="text/javascript"></script>
     <script src="../assets/scripts/jquery.validate.js" type="text/javascript"></script>
 -->
     <script type="text/javascript">

     

  var add_user_form = $("#chat_form").validate({
    rules: {   
        enter_message: "required"
            
    },
    messages: {
      
        enter_message: "Please this field cannot be empty",      
    }
});

</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#chat_box').animate({
        scrollTop: $('#chat_box').get(0).scrollHeight
    }, 2000);
});
</script>