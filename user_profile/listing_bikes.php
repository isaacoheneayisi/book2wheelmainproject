<?php include('header.php') ; 
 $page_title ="list Bike";
 // echo $_SESSION['oauth_uid_fb'];
?>
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<style type="text/css">
  .products {
  display: flex;
  flex-wrap: wrap;
}
.product-card {
  padding: 2%;
  flex-grow: 1;
  flex-basis: 16%;

  display: flex; /* so child elements can use flexbox stuff too! */
}
.product-image img {
  max-width: 100%;
  width: 200px;
  height: 200px;
}

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>



          <div class="col-md-9">
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Listing Bikes</h3>
                </div>
                <div class="panel-body">
                  <!-- Steps form -->
<div class="card">
   <h2> Pending Bikes </h2>
   <hr>
    <div class="card-body mb-4">

   
          <?php 
                   $idd="";
                  $data =array();
                  $id = $_SESSION['id'];
                  //$id = $_SESSION['id'];
                  if ($id) {
                  $idd = $id;
                 }else {
                   $idd = $_SESSION['oauth_uid_fb'];
                  // echo $idd;
                 }
                  $data =get_bike_post($idd);
                  //echo $data;
                  

           ?>
      <div class="row" style="padding-right: 10px; padding-left: 10px">
           <div class="col-md-12">
         <?php   foreach((array)$data as $datas ) {
                  $image ="bikes/".$datas['file'];
                 // echo $image;
                   $id = $datas['id'];
                  $make = $datas['make'];
                  $model = $datas['model'];
                  $displacement= $datas['displacement'];
                  $transM= $datas['transM'];
                  $daily= $datas['daily'];
                  ?>
         <div class="col-md-4">
       
          <table>

              <td>
                <div class="product-image">
         <?php  echo '<img  id="myImg" class="img-thumbnail  modal-content" src="' . $image . '"/>';  ?>

   
             </div>
              <div class="product-info">
       <h5>Make : <?php  echo "<b>".$make."</b"; ?></h5>
      <h5> Model :<?php  echo $model."</b"; ?></h5>
     <h5>Dispalcement : <?php  echo "<b>". $displacement."</b"; ?></h5>
       <h5>Transmission : <?php  echo "<b>".$transM."</b"; ?></h5>
        <h5>1 day Price = <?php  echo "<b>".$daily."</b"; ?></h5> <br />
      
    </div>
             </td>
           
          </table>

  </div>
     <?php } ?>   
</div>
     </div>
       <div id="caption"></div>
    </div>
</div>
<!-- Steps form -->  

<hr>
<div class="card">
   <h2> Approved Bikes</h2>
   <hr>
    <div class="card-body mb-4">
  <?php 
                   $idd="";
                  $data =array();
                  $id = $_SESSION['id'];
                  //$id = $_SESSION['id'];
                  if ($id) {
                  $idd = $id;
                 }else {
                   $idd = $_SESSION['oauth_uid_fb'];
                  // echo $idd;
                 }
                  $bike_data =get_aprroved_bikes($idd);
                  //echo $data;
                  

           ?>
      <div class="row" style="padding-right: 10px; padding-left: 10px">
           <div class="col-md-12">
         <?php   foreach((array)$bike_data as $datass ) {
                  $imagee ="bikes/".$datass['file'];
                 // echo $image;
                  $idd = $datass['idd'];
                  $make = $datass['make'];
                  $model = $datass['model'];
                  $displacement= $datass['displacement'];
                  $transM= $datass['transM'];
                  $daily= $datass['daily'];
                  ?>
         <div class="col-md-4">
       
          <table>

              <td>
                <div class="product-image">
         <?php  echo '<img  id="myImg" class="img-thumbnail  modal-content" src="' . $imagee . '"/>';  ?>

   
             </div>
              <div class="product-info">
       <h5>Make : <?php  echo "<b>".$make."</b"; ?></h5>
      <h5> Model :<?php  echo $model."</b"; ?></h5>
     <h5>Dispalcement : <?php  echo "<b>". $displacement."</b"; ?></h5>
       <h5>Transmission : <?php  echo "<b>".$transM."</b"; ?></h5>
        <h5>1 day Price = <?php  echo "<b>".$daily."</b"; ?></h5> <br />
           <a href="<?php  echo "manage_bike.php?id=".$idd ?>" class="btn btn-danger btn-lg active" style="background-color: #e74c3c: " role="button" aria-pressed="true">Manage Bike</a>


    </div>
             </td>
           
          </table>

  </div>
     <?php } ?>   
</div>
     </div>
       <div id="caption"></div>
   
     
    </div>
</div>              
                        
                </div>
              </div>
          </div>

          <!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
 

 <?php include('footer.php') ; ?>


<!-- The main application script -->

                        
                        