
</section>

<footer id="footer">
      <p>Copyright Book2Wheel, &copy; 2018</p>
    </footer>

    <!-- Modals -->

    <!-- Add Page -->
    <div class="modal fade" id="addPage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Page</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Page Title</label>
          <input type="text" class="form-control" placeholder="Page Title">
        </div>
        <div class="form-group">
          <label>Page Body</label>
          <textarea name="editor1" class="form-control" placeholder="Page Body"></textarea>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox"> Published
          </label>
        </div>
        <div class="form-group">
          <label>Meta Tags</label>
          <input type="text" class="form-control" placeholder="Add Some Tags...">
        </div>
        <div class="form-group">
          <label>Meta Description</label>
          <input type="text" class="form-control" placeholder="Add Meta Description...">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>
 <script>
    // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        //street_number: 'short_name',
        //route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
      

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
      autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
</script>
<!-- AIzaSyDf54m8LLEq0jjMHPe4ijXoojuEBoo5-CM -->
<!--   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf54m8LLEq0jjMHPe4ijXoojuEBoo5-CM&libraries=places"></script> -->


  <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize"></script> -->

<!--   <script>
     CKEDITOR.replace( 'editor1' );
 </script> -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--  <script data-require="bootstrap@*" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link data-require="bootstrap-css@3.1.1" data-semver="3.1.1" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" /> -->
    <script src="js/bootstrap.min.js"></script>
    
    <script src="../assets/scripts/jquery.blockui.min.js" type="text/javascript"></script>

    <!-- <script src="assets/toastr/toastr.min.js" type="text/javascript"></script>   -->
     <script src="../assets/scripts/jquery.validate.min.js" type="text/javascript"></script>
     <script src="../assets/scripts/jquery.validate.js" type="text/javascript"></script>
       <script src="../js/additional-methods.min.js"> </script>
     <script src="js/user_scripts.js"></script>
     <script src="js/bootstrap-datepicker.js"></script>
       <script src="js/loadingoverlay.min.js"></script> 

     <script type="text/javascript">
     // dt = new Date();
      // var d = new Date();
      // var n = d.getFullYear();
     // var year = date.getYear();
      $('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy',
         endDate: '2018'
       });
  </script>

  <?php if (@$page_title == "location") :?>
        <script src="https://maps.googleapis.com/maps/api/js?**v=3;**&key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&libraries=places&callback=initAutocomplete" async defer></script>
    <?php endif; ?>

  <?php if (@$page_title == "Home") :?>
        <script src="https://maps.googleapis.com/maps/api/js?**v=3;**&key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&libraries=places&callback=initAutocomplete" async defer></script>
    <?php endif; ?>

    <?php if (@$page_title == "account") :?>
        <script src="https://maps.googleapis.com/maps/api/js?**v=3;**&key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&libraries=places&callback=initAutocomplete" async defer></script>
    <?php endif; ?>
    <?php if (@$page_title == "add Bike") :?>
        <script src="https://maps.googleapis.com/maps/api/js?**v=3;**&key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&libraries=places&callback=initAutocomplete" async defer></script>

       <script type="text/javascript">
      
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
     function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag1').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
     function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
    $("#profile1-img").change(function(){
        readURL1(this);
    });
     $("#profile2-img").change(function(){
        readURL2(this);
    });
</script>

   

    <?php endif; ?>


  </body>
</html>
