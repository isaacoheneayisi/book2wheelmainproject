<?php include('header.php') ; 
 $page_title ="add Bike";
   
  // echo $_SESSION['oauth_uid_fb'];
  // die ();

?>
<link href="css/fine-uploader-new.css" rel="stylesheet">
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<style type="text/css">
    .steps-form {
    display: table;
    width: 100%;
    position: relative; }
.steps-form .steps-row {
    display: table-row; }
.steps-form .steps-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc; }
.steps-form .steps-row .steps-step {
    display: table-cell;
    text-align: center;
    position: relative; }
.steps-form .steps-row .steps-step p {
    margin-top: 0.5rem; }
.steps-form .steps-row .steps-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important; }
.steps-form .steps-row .steps-step .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
    margin-top: 0; }
    .btn-default {
    background-color: #2bbbad!important;
    color: #fff!important;
}
.btn-indigo {
    background-color: #3f51b5!important;
    color: #fff!important;
}
.btn-rounded {
    border-radius: 10em;
}
.btn {
    padding: .84rem 2.14rem;
    font-size: 20px;
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    margin: .375rem;
    border: 0;
    border-radius: .125rem;
    cursor: pointer;
    text-transform: uppercase;
    white-space: normal;
    word-wrap: break-word;
    color: #fff!important;
    border-radius: 20px;

}



#formdiv {
  text-align: center;
}
#file {
  color: green;
  padding: 5px;
  border: 1px dashed #123456;
  background-color: #f9ffe5;
}
#img {
  width: 17px;
  border: none;
  height: 17px;
  margin-left: -20px;
  margin-bottom: 191px;
}
.upload {
  width: 100%;
  height: 30px;
}
.previewBox {
  text-align: center;
  position: relative;
  width: 150px;
  height: 150px;
  margin-right: 10px;
  margin-bottom: 20px;
  float: left;
}
.previewBox img {
  height: 150px;
  width: 150px;
  padding: 5px;
  border: 1px solid rgb(232, 222, 189);
}
.delete {
  color: red;
  font-weight: bold;
  position: absolute;
  top: 0;
  cursor: pointer;
  width: 20px;
  height:  20px;
  border-radius: 50%;
  background: #ccc;
}

</style>
<style>
        #trigger-upload {
            color: white;
            background-color: #00ABC7;
            font-size: 14px;
            padding: 7px 20px;
            background-image: none;
        }

        #fine-uploader-manual-trigger .qq-upload-button {
            margin-right: 15px;
        }

        #fine-uploader-manual-trigger .buttons {
            width: 36%;
        }

        #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
            width: 60%;
        }
        .error{
             color: red;
        }
        #example1 {
    
    padding: 10px;
    border-radius: 25px;
    background-color: #eeeeee;
}
#personal_information{
        display:none;
    }
#rates_information{
        display:none;
    }
#account_information{
        display:show;
    }
#weeklyy{
        display:none;
        color: red;
        margin-top: 5px;
    }
#monthlyy{
        display:none;
         color: red;
        margin-top: 5px;
    }
    }
    .modal-confirm {        
        color: #636363;
        width: 325px;
    }
    .modal-confirm .modal-content {
        padding: 20px;
        border-radius: 5px;
        border: none;
    }
    .modal-confirm .modal-header {
        border-bottom: none;   
        position: relative;
    }
    .modal-confirm h4 {
        text-align: center;
        font-size: 26px;
        margin: 30px 0 -15px;
    }
    .modal-confirm .form-control, .modal-confirm .btn {
        min-height: 40px;
        border-radius: 3px; 
    }
    .modal-confirm .close {
        position: absolute;
        top: -5px;
        right: -5px;
    }   
    .modal-confirm .modal-footer {
        border: none;
        text-align: center;
        border-radius: 5px;
        font-size: 13px;
    }   
    .modal-confirm .icon-box {
        color: #fff;        
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: -70px;
        width: 95px;
        height: 95px;
        border-radius: 50%;
        z-index: 9;
        background: #82ce34;
        padding: 15px;
        text-align: center;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
    }
    .modal-confirm .icon-box i {
        font-size: 58px;
        position: relative;
        top: 3px;
    }
    .modal-confirm.modal-dialog {
        margin-top: 80px;
    }
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
        background: #82ce34;
        text-decoration: none;
        transition: all 0.4s;
        line-height: normal;
        border: none;
    }
    .modal-confirm .btn:hover, .modal-confirm .btn:focus {
        background: #6fb32b;
        outline: none;
    }
    .trigger-btn {
        display: inline-block;
        margin: 100px auto;
    }


</style>
    </style>
    <style>
.input-group-addon{color:#ff0000; background-color: #ffffff;}
</style>
<script>

function preview_images() 
{
 var total_file=document.getElementById("images").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
 }
}
</script>
<script type="text/javascript">

function otherMake(){  
  var e = document.getElementById("make");
   var strUser = e.options[e.selectedIndex].value;
  // var selected_val = document.getElementById('make').selected;
  if (strUser == "Other") {
   $('#makeOther').show();
  }else{
    $('#makeOther').hide();
  }
  // alert(strUser);
    // if(){
    //   $('#makeOther').show();
    // } 
}

function copyTo(obj) {
   
   var weekVal = obj.value;

   
  var daily = document.getElementById('daily').value;
  var total = daily * 7;

  var calPer ;
  if (daily==""){
    alert ("Please enter daily Amount");
  }
   if (weekVal=="" || weekVal==0){
    alert ("Please enter weekly Amount");
    calPer = (0/daily) * 100;
  }else {
    calPer =((total-weekVal)/total) * 100;
  }
   
   // alert (calPer);
    document.getElementById('weeklyy').style.display = "block";
  // document.getElementById("weeklyy").textContent= "="+" "+ " " +calPer.toFixed(0) +"%"+" "+" discount";

   // $("#weeklyy").html($(this).val(calPer));
}

function copyTomonthly(obj) {
   
   var monthVal = obj.value;
   
  var daily = document.getElementById('daily').value;
   var calPer ;
  if (daily==""){
    alert ("Please enter daily Amount");
  }
   if (monthVal=="" ||monthVal==0 ){
    alert ("Please enter Monthly Amount");
    calPer = (0/daily) * 100;
  }else {
    calPer = (monthVal/(daily*30)) * 100;
  }
   // alert (calPer);
    document.getElementById('monthlyy').style.display = "block";
   //document.getElementById("monthlyy").textContent= "="+" "+ " " +calPer.toFixed(0) +"%"+" "+" discount";

   // $("#weeklyy").html($(this).val(calPer));
}


    $(document).ready(function () {

  $('#makeOther').hide();
  $('#secondPage').hide();
  $('#thirdPage').hide();

$("#next").click(function(){
      var form = $("#addBike");
      form.validate({
    rules: {

        make : {
                  required: true
           },
        model : {
                  required: true
           },
        transM : {
                  required: true
           },
        displacement: {
               required: true,
               number: true,
               minlength: 2,
               maxlength: 4
        } ,
        year : {
            required : true
        } ,
        file: {
              required: true,
             // extension:'jpeg,png',
              //uploadFile:true,
              }

                
    },
    messages: {
        displacement: "Please enter on at lease four numbers" ,
        make: "Please select make" ,    
        transM: "Please field is required" ,  
        model: "Select Model" ,    
        year: "Enter a valid year" ,  
        file : "Please choose a file "   
    }
});
            if (form.valid() == true){
             current_fs = $('#account_information');
            next_fs = $('#personal_information');
           // Show full page LoadingOverlay
                $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 3000);
            current_fs.hide();
            next_fs.show(); 
           $('#secondPage').show();
             $('#firstPage').hide();
            
        }
    });

 $("#next2").click(function(){
    var form = $("#addBike");
      form.validate({
    rules: {

        autocomplete : {
                  required: true
           },
        description_bike : {
                  required: true
           },
           items1: {
            required : true
           }

                
    },
    messages: {
        autocomplete: "Please enter on at lease four numbers" ,
        description_bike: "Please select make" ,    
        items1: "Please at least choose 1 item"    
    }
});
     
      if (form.valid() == true){
             current_fs = $('#personal_information');
            next_fs = $('#rates_information');
           // Show full page LoadingOverlay
                $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 3000);
            current_fs.hide();
            next_fs.show(); 
            $('#secondPage').hide();
            $('#thirdPage').show();
            
        }
    });
     
$("#view_details").click(function(){
      var form = $("#addBike");
     form.validate({
    rules: {

        'insurance_check[]': { 

                        required: true,
                        maxlength: 2
                  },
         weekly : {
                  required: true,
                  number : true
         },
          monthly : {
                  required: true,
                  number : true
         },
          daily : {
                  required: true,
                  number : true
         }
                
    },
    messages: {
        'Insurance[]': "Please choose at least one Insurance policy" ,
      
          
    }
         
});
       

      
      if (form.valid() == true){
             current_fs = $('#personal_information');
            next_fs = $('#rates_information');
           // Show full page LoadingOverlay
                $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 2000);
            current_fs.hide();
           
              var formData = new FormData($('#addBike')[0]);
        var ajaxRequest = $.ajax({
        type: "POST",
        //url: '../includes/manage_post',
       url: '../includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 3000);
        console.log(xhr);
        if(xhr==1) {
          $('#successBtn').modal('toggle');  
           next_fs = $('#rates_information');
           current_fs = $('#account_information');
            next_fs.hide();
            // window.location.href = "listing_bikes.php";
             window.location.href = "listing_bikes.php";
         // $('#addBike')[0].reset();
          //current_fs.show();  
          
           

           // document.getElementById('resQuotes').style.display = "none";
           // document.getElementById('res').style.display = "none";
           // document.getElementById('resShow').style.display = "block";
           
          //toastr.success("Email has been sent succesffully.", "Status");
           //$('#add_insurance')[0].reset();
        }else {
           // toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });
          //  next_fs.show(); 
           // $('#overview').modal('toggle');  
           // var daily = document.getElementById('daily').value;
           // var weekly = document.getElementById('weekly').value;
           // var monthly = document.getElementById('monthly').value;
           // var insu = document.getElementById('Insure_details').value;
           // var term = document.getElementById('terms').value;
           // var daily = document.getElementById('daily').value;
           // var daily = document.getElementById('daily').value;

           //  document.getElementById('dailyPrice').value  = daily;
           //  document.getElementById('weeklyPrice').value  = weekly;
           //  document.getElementById('monthlyPrice').value  = monthly;
           //  document.getElementById('termsCond').value  = term;
           //  document.getElementById('insuD').value  = insu;


        }
    });

     

// $('#next').click(function(){
//             current_fs = $('#account_information');
//             next_fs = $('#personal_information');
//             next_fs.show(); 
//             current_fs.hide();
//         });
// $('#next2').click(function(){
//             current_fs = $('#personal_information');
//             next_fs = $('#rates_information');
//             next_fs.show(); 
//             current_fs.hide();
//         });
 $('#previous').click(function(){
            current_fs = $('#personal_information');
            next_fs = $('#account_information');
            next_fs.show(); 
            current_fs.hide();
            $('#secondPage').show();
            $('#firstPage').hide();
 });

  $('#previous2').click(function(){
            current_fs = $('#rates_information');
            next_fs = $('#personal_information');
            next_fs.show(); 
            current_fs.hide();
            var $target = $($(this).attr('href'));
            $target.show();
            $('#thirdPage').hide();
            $('#secondPage').show();
 });




 //    var navListItems = $('div.setup-panel div a'),
 //        allWells = $('.setup-content'),
 //        allNextBtn = $('.nextBtn'),
 //        allPrevBtn = $('.prevBtn');  

 //    allWells.hide();

 //    navListItems.click(function (e) {
 //        e.preventDefault();
 //        var $target = $($(this).attr('href')),
 //            $item = $(this);

 //        if (!$item.hasClass('disabled')) {
 //            navListItems.removeClass('btn-indigo').addClass('btn-default');
 //            $item.addClass('btn-indigo');
 //            allWells.hide();
 //            $target.show();
 //            $target.find('input:eq(0)').focus();
 //        }
 //    });
  
 //    allPrevBtn.click(function(){
 //        var curStep = $(this).closest(".setup-content"),
 //            curStepBtn = curStep.attr("id"),
 //            prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
 //            //Show full page LoadingOverlay
 //                $.LoadingOverlay("show");

 //                // Hide it after 3 seconds
 //                setTimeout(function(){
 //                    $.LoadingOverlay("hide");
 //                }, 3000);

 //            prevStepSteps.removeAttr('disabled').trigger('click');
 //    });

 //    allNextBtn.click(function(){
        
 //        var curStep = $(this).closest(".setup-content"),
 //            curStepBtn = curStep.attr("id"),
 //            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
 //            curInputs = curStep.find("input[type='text'],input[type='url'],select"),            
 //            isValid = true;

 //        $(".form-group").removeClass("has-error");
 //        for(var i=0; i< curInputs.length; i++){
          
 //            if (!curInputs[i].validity.valid){
               
 //                isValid = false;
 //            $(curInputs[i]).closest(".form-group").addClass("has-error");

           
 //            }
 //        }
     
 //        if (isValid)
 //            // if ($( "div" ).hasClass( "img" ).attr() == ""){
 //            //     alert("please choose an image");
 //            //     return false;
 //            // }
 // //if ($("#addBike").valid()){
         

 //            //

 //            nextStepWizard.removeAttr('disabled').trigger('click');
 //             //Show full page LoadingOverlay
 //                $.LoadingOverlay("show");

 //                // Hide it after 3 seconds
 //                setTimeout(function(){
 //                    $.LoadingOverlay("hide");
 //                }, 3000);
 //             // $('.error').hide();
 //       //   }
            
 //    });

    //$('div.setup-panel div a.btn-indigo').trigger('click');
});
</script>


<!-- Fine Uploader Thumbnails template w/ customization
    ====================================================================== -->
<!--     <script type="text/template" id="qq-template-manual-trigger">
        <div class="qq-uploader-selector qq-uploader"  qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>Select files</div>
                </div>
                <button type="button" id="trigger-upload" class="btn btn-primary">
                    <i class="icon-upload icon-white"></i> Upload
                </button>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script> -->
<!-- Steps form -->

          <div class="col-md-9">
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Add Bike</h3>
                </div>
                <div class="panel-body">
                  <!-- Steps form -->
<div class="card">
    <div class="card-body mb-4">
        <!-- Stepper -->
        <div class="steps-form" id=firstPage>
            <div class="steps-row setup-panel">
                <div class="steps-step">
                    <span class="btn btn-indigo btn-circle" >1</span>  
                    <p>Step 1</p>
                </div>
                <div class="steps-step">
                     <span class="btn btn-indigo btn-circle disabled" >2</span>  
                    <p>Step 2</p>
                </div>
                <div class="steps-step">
                      <span class="btn btn-indigo btn-circle disabled" >3</span>  
                    <p>Step 3</p>
                </div>
            </div>
        </div>
         <div class="steps-form" id=secondPage>
            <div class="steps-row setup-panel">
                <div class="steps-step">
                    <span class="btn btn-indigo btn-circle" >1</span>  
                    <p>Step 1</p>
                </div>
                <div class="steps-step">
                     <span class="btn btn-indigo btn-circle " >2</span>  
                    <p>Step 2</p>
                </div>
                <div class="steps-step">
                      <span class="btn btn-indigo btn-circle disabled" >3</span>  
                    <p>Step 3</p>
                </div>
            </div>
        </div>
        
         <div class="steps-form" id=thirdPage>
            <div class="steps-row setup-panel">
                <div class="steps-step">
                    <span class="btn btn-indigo btn-circle" >1</span>  
                    <p>Step 1</p>
                </div>
                <div class="steps-step">
                     <span class="btn btn-indigo btn-circle " >2</span>  
                    <p>Step 2</p>
                </div>
                <div class="steps-step">
                      <span class="btn btn-indigo btn-circle" >3</span>  
                    <p>Step 3</p>
                </div>
            </div>
        </div>
        <form role="form"  id="addBike" method="post"  enctype="multipart/form-data" >
       <input type="hidden" name="opera" value="addBike">
          <fieldset id="account_information" class="">
            <!-- First Step -->
            <div class="row setup-content" id="step-9">
                   <div class="row" style="padding-right: 10px; padding-left: 10px">
                <div class="col-md-6">

                    <!-- <h3 class="font-weight-bold pl-0 my-4"><strong>Add Bike</strong></h3> -->
                    <div class="form-group md-form">
                        <label for="yourName" data-error="wrong" data-success="right">Make</label>
                       <select class="form-control validate custom_select_class" name="make"  id="make" required
                                                data-parsley-required-message="This field is required." onchange="otherMake()">
                                            <option value="">Select an option</option>
                                                                                                <option
                                                        value="Aprilia">Aprilia</option>

                                                                                                        <option
                                                        value="BMW">BMW</option>
                                                                                                        <option
                                                        value="3">Buell</option>
                                                                                                        <option
                                                        value="Buell">Can-Am</option>
                                                                                                        <option
                                                        value="Ducati">Ducati</option>
                                                                                                        <option
                                                        value="Harley Davidson">Harley Davidson</option>
                                                                                                        <option
                                                        value="Honda">Honda</option>
                                                                                                        <option
                                                        value="Kawasaki">Kawasaki</option>
                                                                                                        <option
                                                        value="KTM">KTM</option>
                                                                                                        <option
                                                        value="Kymco">Kymco</option>
                                                                                                        <option
                                                        value="Moto Guzzi">Moto Guzzi</option>
                                                                                                        <option
                                                        value="Suzuki">Suzuki</option>
                                                                                                        <option
                                                        value="Triumph">Triumph</option>
                                                                                                        <option
                                                        value="Victory">Victory</option>
                                                                                                        <option
                                                        value="Yamaha">Yamaha</option>
                                                                                                        <option
                                                        value="BSA">BSA</option>
                                                                                                        <option
                                                        value="Janus">Janus</option>
                                                                                                        <option
                                                        value="Ural">Ural</option>
                                                                                                        <option
                                                        
                                                        value="Indian">Indian</option>
                                                         <option
                                                        
                                                        value="Other">Other</option>
                                                                                            </select>
                                                                                            <div id="makeOther">
                         <label for="yourLastName" data-error="wrong" data-success="right" style="color:red">Please specify</label>
                        <input id="yourLastName" type="text" required="required" id="make_ent" name="make_ent" class="form-control validate" placeholder="Enter your Make">
                      </div>
                    </div>
                    <div class="form-group md-form mt-3">
                        <label for="yourLastName" data-error="wrong" data-success="right">Model</label>
                        <input id="yourLastName" type="text" required="required" id="model" name="model" class="form-control validate" placeholder="Enter motobike mode e.g CBR ">
                    </div>
                    <div class="form-group md-form mt-3">
                        <label for="yourAddress" data-error="wrong" data-success="right">Year</label>

                         <input class="date-own form-control validate" style="width: 300px;" type="text" required="required" id="year" name="year" >

                    </div>
                   
                </div>
                  <div class="col-md-6">

                    <!-- <h3 class="font-weight-bold pl-0 my-4"><strong>Step 1</strong></h3> -->
                    <div class="form-group md-form">
                        <label for="yourName" data-error="wrong" data-success="right">Transmission</label>
                        <select class="form-control validate custom_select_class" name="transM"
                                                id="transM" required
                                                data-parsley-required-message="This field is required.">
                                            <option value="">Select an Option</option>
                                                                                                <option
                                                        value="Automatic">Automatic</option>
                                                        
                                                                                                        <option
                                                        value="Semi Automatic">Semi Automatic</option>
                                                                                                        <option
                                                        value="Manual">Manual</option>
                                                                                                      
                                                        
                                                                                                       
                                                                                                    </select>
                    </div>
                    <div class="form-group md-form mt-3">
                        <label for="yourLastName" data-error="wrong" data-success="right">Displacement</label>
                        <div class="input-group">
                        
                        <input type="text" required="required" class="form-control " id="displacement" name="displacement" placeholder="Please enter your CC">
                        <div class="input-group-addon">
                          CC
                      </div>
                        </div>
                    </div>
                
                    <div class="form-group md-form mt-3">
                        <label for="yourAddress" data-error="wrong" data-success="right">PlateNumber</label>
                        <input id="yourLastName" type="text" required="required" id="PlateNumber" name="PlateNumber" class="form-control validate" placeholder="Enter PlateNumber e.g ABCD123456">
                    </div>
                    
                </div>
            </div>
              
             <div class="row" style="padding-right: 10px; padding-left: 25px">
                
                  <div class="col-md-12">
                      <div class="col-md-4">
                          <input type="file" name="file" id="profile-img" style="color: #FFF">
                           <img src="" id="profile-img-tag" width="200px" />
                       
                      </div>
                       <div class="col-md-4">
                         <input type="file" name="file1" id="profile1-img">
                           <img src="" id="profile-img-tag1" width="200px" />
                         
                      </div>
                       <div class="col-md-4">
                          <input type="file" name="file2" id="profile2-img">
                           <img src="" id="profile-img-tag2" width="200px" />
                          
                      </div>
                 
                    
                            </div>
             </div>
<!-- 
             <div class="row" style="padding-right: 10px; padding-left: 10px">
                  <div class="col-md-12">
                    <img src="img/phone.png" width="100%">
                  </div>
              </div> -->
                <button class="btn btn-indigo btn-rounded nextBtn float-right" type="button" id="next" >Next</button>
            </div>
    
    </fieldset>

<fieldset id="personal_information" class="">
            <!-- Second Step -->
             
            <div class="row setup-content" id="step-10">
              <div class="row" >
                <div class="col-md-12">                    
                  <div>
                      
                  <div class="col-md-6" style="margin-top: 10px">
                     <h4 class="font-weight-bold pl-0 my-4" style="margin:auto"><strong>Items you Provide</strong></h4>
                    <div class="form-group md-form" style="margin-top: 10px">
                        <div>
                       <div class="form-check"  style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input validate" id="items1" name="items1" value="2 Helmet">
                            <label class="form-check-label" for="checkbox101">2 Helmet</label>
                        </div>
                        <div class="form-check" style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input" id="items2" name="items2" value="Glasses">
                            <label class="form-check-label" for="checkbox101">Glasses</label>
                        </div>
                               
                               <div class="form-check" style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input" id="items3" name="items3" value="Compartment">
                            <label class="form-check-label" for="checkbox101">Compartment</label>
                        </div>
                               
                               <div class="form-check" style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input" id="items4" name="items4" value="Gloves">
                            <label class="form-check-label" for="checkbox101">Gloves</label>
                        </div>
                         <div class="form-check" style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input" id="items5" name="items5" value="Sin Card" >
                            <label class="form-check-label" for="checkbox101">Sin Card</label>
                        </div>
                         <div class="form-check" style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input" id="items6" name="items6" value="Lock">
                            <label class="form-check-label" for="checkbox101">Lock</label>
                        </div>

                    </div>
                    <div style="margin-top: 10px">
                        <textarea class="form-control validate" required="required" id="description_bike" name="description_bike" rows="3" style="height: 220px" placeholder="Motobike description"></textarea>
                    </div>
                               
                    </div>
                    
                    </div>
                     <div class="col-md-6" style="margin-top: 10px">
                        <h4 class="font-weight-bold pl-0 my-4" style="margin:auto"><strong>Bike Location</strong></h4>
                        <div id="address">
                    <div class="form-group md-form" style="margin-top: 10px">
                        <label for="companyName" data-error="wrong" data-success="right">Address</label>
                        <input  type="text" required="required" class="form-control validate" id="autocomplete" name="autocomplete" placeholder="Enter your address"  onFocus="geolocate()">
                    </div>
                     
                 <!--    <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">Route</label>
                        <input id="route" name="route" type="text"  class="form-control " readonly="">
                    </div> -->

                    <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">City</label>
                        <input id="locality"  required="required" type="text" name="locality"  class="form-control" >
                    </div>
                      <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">State</label>
                        <input id="administrative_area_level_1" type="text" name="administrative_area_level_1"  class="form-control" readonly="">
                    </div>
 
                   
                     <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">Country</label>
                        <input id="country" required="required" type="text" name="country" r class="form-control " >
                       
                    </div>
                     <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">Zip Code</label>
                        <input id="postal_code" type="text" name="postal_code" class="form-control" readonly="">
                    </div>
                </div>
                    </div>

                    <button class="btn btn-indigo btn-rounded prevBtn float-left" type="button" id="previous">Previous</button>
                    <button class="btn btn-indigo btn-rounded nextBtn float-right" type="button" id="next2">Next</button>
                
            </div>
            </div>
            </div>
          </div>
      </fieldset>

            <!-- Third Step -->

               <fieldset id="rates_information" class="">
            <div class="row setup-content" id="step-11">
                <div class="col-md-12">
                
                    <div class="row" >
                <div class="col-md-12">                    
                  <div>
                        <div class="col-md-6" style="margin-top: 10px">
                        <h4 class="font-weight-bold pl-0 my-4" style="margin:auto"><strong>Your Rates (PHP)</strong></h4>
                        <div id="address">
                    <div class="form-group md-form" style="margin-top: 10px">
                        <label for="companyName" data-error="wrong" data-success="right">1 day Price</label>
                        <input  type="text" required="required"  class="form-control validate" id="daily" name="daily" placeholder="Enter daily amount" >
                    </div>
                     
                    <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">7 days Price</label>
                        <input placeholder="Enter weekly amount" name="weekly" id="weekly" required="required"  onkeyup="copyTo(this)" type="text"  class="form-control validate" >
                        <label  id="weeklyy"></label>
                    </div>

                    <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">30 days Price</label>
                        <input id="monthly" type="text"  name="monthly" required="required"  onkeyup="copyTomonthly(this)"  class="form-control validate" placeholder="Enter monthly amount" >
                         <label id="monthlyy"></label>
                    </div>
                     
 
                </div>
                    </div>
                  <div class="col-md-6" style="margin-top: 10px">
                     <h4 class="font-weight-bold pl-0 my-4" style="margin:auto"><strong>Insurance</strong></h4>
                    <div class="form-group md-form" style="margin-top: 10px">
                        <div>
                       <div class="form-check"  style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input validate" id="insurance_check1" name="insurance_check1" value="Third Party Insurance" >
                            <label class="form-check-label " for="checkbox101">Third Party Insurance</label>
                        </div>
                        <div class="form-check" style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input" id="insurance_check2" name="insurance_check2" value="Personal Insurance">
                            <label class="form-check-label" for="checkbox101">Personal Insurance</label>
                        </div>
                               
                               <div class="form-check" style="margin-bottom: 5px">
                            <input type="checkbox" class="filled-in form-check-input" id="insurance_check3" name="insurance_check3" value="Extra Insurance">
                            <label class="form-check-label" for="checkbox101">Extra Insurance</label>
                        </div>                               
                           

                    </div>
                    <div style="margin-top: 10px">
                        <label for="companyAddress" data-error="wrong" data-success="right">Insurance Details</label>
                        <textarea class="form-control validate" required="required"  id="Insure_details" name="Insure_details" rows="3" style="height: 100px" placeholder="Insurance Details"></textarea>
                    </div>
                               
                    </div>
                    
                    </div>
                      <div class="col-md-12" style="margin-top: 10px">
                        <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">Terms and conditions</label>
                         <textarea class="form-control validate" required="required"  id="terms" name="terms" rows="3" style="height: 100px" placeholder="Here you can write terms which rentee must accept and follow"></textarea>
                    </div>
                     
                   


                      </div>
                   

                    
            </div>
            </div>
            </div>


                    <button class="btn btn-indigo btn-rounded prevBtn float-left" type="button" id="previous2">Back</button>
                    <button class="btn btn-default btn-rounded float-right" type="button"  id="view_details">Submit </button>
                </div>
            </div>
        </fieldset>
            
        </form>

    </div>
</div>
<!-- Steps form -->                
                        
                </div>
              </div>
          </div>
  

  <!-- Modal HTML -->
<div id="successBtn" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="material-icons">&#xE876;</i>
                </div>              
                <h4 class="modal-title">Awesome!</h4>   
            </div>
            <div class="modal-body">
                <p class="text-center">You have successfully added a bike . The Admin team will examine your post before been published.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>     
  
    <!-- Full Height Modal Right Success-->
         
            <!-- Full Height Modal Right Success-->
  <!-- Central Modal Medium Success-->
  
                                                                 s


            <script>
        // var manualUploader = new qq.FineUploader({
        //     element: document.getElementById('fine-uploader-manual-trigger'),
        //     template: 'qq-template-manual-trigger',
        //     request: {
        //         endpoint: 'server/uploads/IncrementalFileUpload.php'
        //     },
        //     chunking: {
        //         enabled: true,
        //         concurrent: {
        //             enabled: true
        //         },
        //         success: {
        //             endpoint: "server/chunksdone"
        //         }
        //     },
        //     thumbnails: {
        //         placeholders: {
        //             waitingPath: 'placeholders/waiting-generic.png',
        //             notAvailablePath: 'placeholders/not_available-generic.png'
        //         }
        //     },
        //      validation: {
        //         allowedExtensions: ['jpeg', 'jpg', 'txt','png'],
        //         itemLimit: 3,
        //         //sizeLimit: 100200 ,// 50 kB = 50 * 1024 bytes
        //         resize : true,
        //         maxWidth:500 ,
        //         maxHeight:500,
        //         itemLimit: 3

        //     },
            
        //     autoUpload: false,
        //     debug: true
        // });

        // qq(document.getElementById("trigger-upload")).attach("click", function() {
        //     manualUploader.uploadStoredFiles();
        // });
    </script>
 <?php include('footer.php') ; ?>


<!-- The main application script -->

                        
                        