<?php include('header.php') ; 
 $page_title ="Bookings";
?>
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<style type="text/css">
  .products {
  display: flex;
  flex-wrap: wrap;
}
.product-card {
  padding: 2%;
  flex-grow: 1;
  flex-basis: 16%;

  display: flex; /* so child elements can use flexbox stuff too! */
}
.product-image img {
  max-width: 100%;
  width: 200px;
  height: 200px;
}

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
.bs-example
{
    margin: 30px;
    height: 120px;
    overflow: scroll;
}
.dropdown-menu{
    position:fixed;
}

.button{
    height:30px;
}
</style>



          <div class="col-md-9">
               <div class="panel panel-default">
              <!--   <div class="panel-heading">
                  <h3 class="panel-title">Listing Bikes</h3>
                </div> -->
                <div class="panel-body">
                  <!-- Steps form -->
<div class="card">
   <h2> Bookings</h2>
   <hr>
        <!-- <div id="view_approved_bikes"> </div> -->
             
                        
                </div>
                    </div>
                        </div>
            </div>


          <!-- The Modal -->

<div class="modal fade" id="confirm-approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Approval</h4>
                </div>
            
                <div class="modal-body">
                    <p>You are about to approve and publish a bike . </p>
                    <p>Do you want to proceed?</p>
                    <form role="form"  id="app_bike" method="post"  enctype="multipart/form-data" >
                    <input type="hidden" name="opera" value="approve_bike">
                    <input type="hidden" name="user_id" id="user_id" value="" />

                    <p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                    <a class="btn btn-danger btn-ok" >Approve Bike</a>
                     </form>
                </div>
            </div>
        </div>
    </div>

 <?php include('footer.php') ; ?>

    <script data-require="bootstrap@*" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link data-require="bootstrap-css@3.1.1" data-semver="3.1.1" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
 <script src="https://code.jquery.com/jquery-1.12.4.js/loadingoverlay.min.js"></script> 
 <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="js/bookings.js"></script> 
<!-- The main application script -->
<script type="text/javascript">
  $('.dropdown-toggle').click(function (){
            dropDownFixPosition($('button'),$('.dropdown-menu'));
        });
function dropDownFixPosition(button,dropdown){
      var dropDownTop = button.offset().top + button.outerHeight();
        dropdown.css('top', dropDownTop + "px");
        dropdown.css('left', button.offset().left + "px");
}
</script>
           
   