<?php include('header.php') ; 
 $page_title ="list Bike";
?>
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<style type="text/css">
  .products {
  display: flex;
  flex-wrap: wrap;
}
.product-card {
  padding: 2%;
  flex-grow: 1;
  flex-basis: 16%;

  display: flex; /* so child elements can use flexbox stuff too! */
}
.product-image img {
  max-width: 100%;
  width: 200px;
  height: 200px;
}

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
.bs-example
{
    margin: 30px;
    height: 120px;
    overflow: scroll;
}
.dropdown-menu{
    position:fixed;
}

.button{
    height:30px;
}
</style>



          <div class="col-md-9">
               <div class="panel panel-default">
              <!--   <div class="panel-heading">
                  <h3 class="panel-title">Listing Bikes</h3>
                </div> -->
                <div class="panel-body">
                  <!-- Steps form -->
<div class="card">
   <h2> <a href="approve_bikes.php" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Previous</a>
Bike Profile</h2>
   <hr>
       <?php   
                 $data =  get_bike_per_id($_GET['id']);
                  $idd = $_GET['id'];
               foreach((array) $data as $datas) {
                 $make = $datas['make'];
                 $model = $datas['model'];
                 $year = $datas['year_bike'];
                 $transM = $datas['transM'];
                 $displacement = $datas['displacement'];
                  $PlateNumber = $datas['PlateNumber'];

       ?>
               <form role="form"  id="bike_profile_updates" method="post"  enctype="multipart/form-data" >
       <input type="hidden" name="opera" value="update_bike_profile">
       <input type="hidden" name="idd" value="<?php echo $idd;  ?>">
          <fieldset id="account_information" class="">
            <!-- First Step -->
            <div class="row setup-content" id="step-9">
                   <div class="row" style="padding-right: 10px; padding-left: 10px">
                <div class="col-md-6">

                    <!-- <h3 class="font-weight-bold pl-0 my-4"><strong>Add Bike</strong></h3> -->
                    <div class="form-group md-form">
                        <label for="yourName" data-error="wrong" data-success="right">Make</label>
                       <select class="form-control validate custom_select_class" name="make"  id="make" required
                                                data-parsley-required-message="This field is required." onchange="otherMake()">
                                            <option value=""><?php  echo  $make ;  ?></option>
                                                                                                <option
                                                        value="Aprilia">Aprilia</option>

                                                                                                        <option
                                                        value="BMW">BMW</option>
                                                                                                        <option
                                                        value="3">Buell</option>
                                                                                                        <option
                                                        value="Buell">Can-Am</option>
                                                                                                        <option
                                                        value="Ducati">Ducati</option>
                                                                                                        <option
                                                        value="Harley Davidson">Harley Davidson</option>
                                                                                                        <option
                                                        value="Honda">Honda</option>
                                                                                                        <option
                                                        value="Kawasaki">Kawasaki</option>
                                                                                                        <option
                                                        value="KTM">KTM</option>
                                                                                                        <option
                                                        value="Kymco">Kymco</option>
                                                                                                        <option
                                                        value="Moto Guzzi">Moto Guzzi</option>
                                                                                                        <option
                                                        value="Suzuki">Suzuki</option>
                                                                                                        <option
                                                        value="Triumph">Triumph</option>
                                                                                                        <option
                                                        value="Victory">Victory</option>
                                                                                                        <option
                                                        value="Yamaha">Yamaha</option>
                                                                                                        <option
                                                        value="BSA">BSA</option>
                                                                                                        <option
                                                        value="Janus">Janus</option>
                                                                                                        <option
                                                        value="Ural">Ural</option>
                                                                                                        <option
                                                        
                                                        value="Indian">Indian</option>
                                                         <option
                                                        
                                                        value="Other">Other</option>
                                                                                            </select>
                                                                                           <!--  <div id="makeOther">
                         <label for="yourLastName" data-error="wrong" data-success="right" style="color:red">Please specify</label>
                        <input id="yourLastName" type="text" required="required" id="make_ent" name="make_ent" class="form-control validate" placeholder="Enter your Make">
                      </div> -->
                    </div>
                    <div class="form-group md-form mt-3">
                        <label for="yourLastName" data-error="wrong" data-success="right">Model</label>
                        <input id="yourLastName" type="text" required="required" id="model" name="model" class="form-control validate" placeholder="Enter motobike mode e.g CBR " value="<?php  echo  $model ;  ?>">
                    </div>
                    <div class="form-group md-form mt-3">
                        <label for="yourAddress" data-error="wrong" data-success="right">Year</label>

                         <input class="date-own form-control validate" style="width: 300px;" type="text" required="required" id="year" name="year" value="<?php   echo $year; ?>" >

                    </div>
                   
                </div>
                  <div class="col-md-6">

                    <!-- <h3 class="font-weight-bold pl-0 my-4"><strong>Step 1</strong></h3> -->
                    <div class="form-group md-form">
                        <label for="yourName" data-error="wrong" data-success="right">Transmission</label>
                        <select class="form-control validate custom_select_class" name="transM"
                                                id="transM" required
                                                data-parsley-required-message="This field is required.">
                                            <option value=""><?php  echo $transM; ?></option>
                                                                                                <option
                                                        value="Automatic">Automatic</option>
                                                        
                                                                                                        <option
                                                        value="Semi Automatic">Semi Automatic</option>
                                                                                                        <option
                                                        value="Manual">Manual</option>
                                                                                                      
                                                        
                                                                                                       
                                                                                                    </select>
                    </div>
                    <div class="form-group md-form mt-3">
                        <label for="yourLastName" data-error="wrong" data-success="right">Displacement</label>
                        <div class="input-group">
                        
                        <input type="text" required="required" class="form-control " id="displacement" name="displacement" placeholder="Please enter your CC" value="<?php echo $displacement; ?>">
                        <div class="input-group-addon">
                          CC
                      </div>
                        </div>
                    </div>
                
                    <div class="form-group md-form mt-3">
                        <label for="yourAddress" data-error="wrong" data-success="right">PlateNumber</label>
                        <input id="yourLastName" type="text" required="required" id="PlateNumber" name="PlateNumber" class="form-control validate" placeholder="Enter PlateNumber e.g ABCD123456" value="<?php  echo $PlateNumber; ?>">
                    </div>
                    <?php   } ?>
                </div>
            </div>
                    <?php   
                             
                    $data =  get_bike_per_id($_GET['id']);

        foreach((array)$data as $datas ) {
                 // $image ="user_profile/bikes/".$datas['file'];
                 // echo $image;
                   $id = $datas['id'];
                  $file = $datas['file'];
                  $file1 = $datas['file1'];
                   $file2= $datas['file2'];

                 //  if ($file1 == ""){
                 //   $file11="";
                 //  }else{
                 //    $file112=$file1;
                 //  }
                 // if ($file2 == ""){
                 //   $file21="";
                 //  }else{
                 //    $file212=$file2;
                 //  }
                 //   $arr = array();
                 //  if ($file112=="" && $file212==""){
                 //     $arr = array($file);
                 //  } else if ($file112 == "" && $file212 !==""){
                 //     $arr = array($file, $file212);
                 //  }else if ($file212 == "" && $file112 !==""){
                 //   $arr = array($file, $file112);
                 //  } else {
                 //    $arr = array($file, $file112, $file212);
                 //  }
                  
                  // $transM= $datas['transM'];
                  // $daily= $datas['daily'];
                 

     ?>
             <div class="row" style="padding-
/<?php  echo $file ?>right: 10px; padding-left: 25px">
              
               
                
                  <div class="col-md-12">
                    
                    
                      <div class="col-md-4">
                          <input type="file" name="file" id="profile-img" style="color: #FFF">
                           <img src="bikes/<?php  echo $file ?>" id="profile-img-tag" width="200px" />
                       
                      </div>
                       <div class="col-md-4">
                         <input type="file" name="file1" id="profile1-img">
                           <img src="bikes/<?php  echo $file1 ?>" id="profile-img-tag1" width="200px" />
                         
                      </div>
                       <div class="col-md-4">
                          <input type="file" name="file2" id="profile2-img">
                           <img src="bikes/<?php  echo $file2 ?>" id="profile-img-tag2" width="200px" />
                          
                      </div>
                 
                    
                      </div>
                      <?php   }  ?>
             </div>
             <br><br><br><br>
<!-- 
             <div class="row" style="padding-right: 10px; padding-left: 10px">
                  <div class="col-md-12">
                    <img src="img/phone.png" width="100%">
                  </div>
              </div> -->
               <button type="button" class="btn btn-primary" id="bike_pro_edit">Save Modification</button>
            </div>
    
    </fieldset>
             </form>           
                </div>
                    </div>
                        </div>
            </div>


          <!-- The Modal -->



 <?php include('footer.php') ; ?>



<!--  <script src="https://code.jquery.com/jquery-1.12.4.js/loadingoverlay.min.js"></script>  -->
 <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="js/view_bikes.js"></script> 
<!-- The main application script -->
<script type="text/javascript">
  $('.dropdown-toggle').click(function (){
            dropDownFixPosition($('button'),$('.dropdown-menu'));
        });
function dropDownFixPosition(button,dropdown){
      var dropDownTop = button.offset().top + button.outerHeight();
        dropdown.css('top', dropDownTop + "px");
        dropdown.css('left', button.offset().left + "px");
}
</script>
           
<script type="text/javascript">
      
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
     function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag1').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
     function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
    $("#profile1-img").change(function(){
        readURL1(this);
    });
     $("#profile2-img").change(function(){
        readURL2(this);
    });
</script>
<script type="text/javascript">
       
$("#edit_details").click(function(){
      var form = $("#add_Bike");

      if (form.valid() == true){
            //  current_fs = $('#personal_information');
            // next_fs = $('#rates_information');
           // Show full page LoadingOverlay
                $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 2000);
            // current_fs.hide();
           
         var formData = new FormData($('#addBike')[0]);
        var ajaxRequest = $.ajax({
        type: "POST",
        //url: '../includes/manage_post',
       url: '../includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 3000);
        console.log(xhr);
        if(xhr==1) {
          alert('Bike profile has been updated')
          // $('#successBtn').modal('toggle');  
          //  next_fs = $('#rates_information');
          //  current_fs = $('#account_information');
          //   next_fs.hide();
            // window.location.href = "listing_bikes.php";
             window.location.href = "approve_bikes.php";
         // $('#addBike')[0].reset();
          //current_fs.show();  
          
           

           // document.getElementById('resQuotes').style.display = "none";
           // document.getElementById('res').style.display = "none";
           // document.getElementById('resShow').style.display = "block";
           
          //toastr.success("Email has been sent succesffully.", "Status");
           //$('#add_insurance')[0].reset();
        }else {
           // toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });
          //  next_fs.show(); 
</script>
                           