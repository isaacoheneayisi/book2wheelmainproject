<?php include('header.php') ; 
 $page_title ="list Bike";
?>
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<style type="text/css">
  .products {
  display: flex;
  flex-wrap: wrap;
}
.product-card {
  padding: 2%;
  flex-grow: 1;
  flex-basis: 16%;

  display: flex; /* so child elements can use flexbox stuff too! */
}
.product-image img {
  max-width: 100%;
  width: 200px;
  height: 200px;
}

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
.bs-example
{
    margin: 30px;
    height: 120px;
    overflow: scroll;
}
.dropdown-menu{
    position:fixed;
}

.button{
    height:30px;
}
</style>

<script type="text/javascript">

function otherMake(){  
  var e = document.getElementById("make");
   var strUser = e.options[e.selectedIndex].value;
  // var selected_val = document.getElementById('make').selected;
  if (strUser == "Other") {
   $('#makeOther').show();
  }else{
    $('#makeOther').hide();
  }
  // alert(strUser);
    // if(){
    //   $('#makeOther').show();
    // } 
}

function copyTo(obj) {
   
   var weekVal = obj.value;

   
  var daily = document.getElementById('daily').value;
  var total = daily * 7;

  var calPer ;
  if (daily==""){
    alert ("Please enter daily Amount");
  }
   if (weekVal=="" || weekVal==0){
    alert ("Please enter weekly Amount");
    calPer = (0/daily) * 100;
  }else {
    calPer =((total-weekVal)/total) * 100;
  }
   
   // alert (calPer);
    document.getElementById('weeklyy').style.display = "block";
   document.getElementById("weeklyy").textContent= "="+" "+ " " +calPer.toFixed(0) +"%"+" "+" discount";

   // $("#weeklyy").html($(this).val(calPer));
}

function copyTomonthly(obj) {
   
   var monthVal = obj.value;
   
  var daily = document.getElementById('daily').value;
   var calPer ;
  if (daily==""){
    alert ("Please enter daily Amount");
  }
   if (monthVal=="" ||monthVal==0 ){
    alert ("Please enter Monthly Amount");
    calPer = (0/daily) * 100;
  }else {
    calPer = (monthVal/(daily*30)) * 100;
  }
   // alert (calPer);
    document.getElementById('monthlyy').style.display = "block";
   document.getElementById("monthlyy").textContent= "="+" "+ " " +calPer.toFixed(0) +"%"+" "+" discount";

   // $("#weeklyy").html($(this).val(calPer));
}


    $(document).ready(function () {

  $('#makeOther').hide();
  $('#secondPage').hide();
  $('#thirdPage').hide();





 //    var navListItems = $('div.setup-panel div a'),
 //        allWells = $('.setup-content'),
 //        allNextBtn = $('.nextBtn'),
 //        allPrevBtn = $('.prevBtn');  

 //    allWells.hide();

 //    navListItems.click(function (e) {
 //        e.preventDefault();
 //        var $target = $($(this).attr('href')),
 //            $item = $(this);

 //        if (!$item.hasClass('disabled')) {
 //            navListItems.removeClass('btn-indigo').addClass('btn-default');
 //            $item.addClass('btn-indigo');
 //            allWells.hide();
 //            $target.show();
 //            $target.find('input:eq(0)').focus();
 //        }
 //    });
  
 //    allPrevBtn.click(function(){
 //        var curStep = $(this).closest(".setup-content"),
 //            curStepBtn = curStep.attr("id"),
 //            prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
 //            //Show full page LoadingOverlay
 //                $.LoadingOverlay("show");

 //                // Hide it after 3 seconds
 //                setTimeout(function(){
 //                    $.LoadingOverlay("hide");
 //                }, 3000);

 //            prevStepSteps.removeAttr('disabled').trigger('click');
 //    });

 //    allNextBtn.click(function(){
        
 //        var curStep = $(this).closest(".setup-content"),
 //            curStepBtn = curStep.attr("id"),
 //            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
 //            curInputs = curStep.find("input[type='text'],input[type='url'],select"),            
 //            isValid = true;

 //        $(".form-group").removeClass("has-error");
 //        for(var i=0; i< curInputs.length; i++){
          
 //            if (!curInputs[i].validity.valid){
               
 //                isValid = false;
 //            $(curInputs[i]).closest(".form-group").addClass("has-error");

           
 //            }
 //        }
     
 //        if (isValid)
 //            // if ($( "div" ).hasClass( "img" ).attr() == ""){
 //            //     alert("please choose an image");
 //            //     return false;
 //            // }
 // //if ($("#addBike").valid()){
         

 //            //

 //            nextStepWizard.removeAttr('disabled').trigger('click');
 //             //Show full page LoadingOverlay
 //                $.LoadingOverlay("show");

 //                // Hide it after 3 seconds
 //                setTimeout(function(){
 //                    $.LoadingOverlay("hide");
 //                }, 3000);
 //             // $('.error').hide();
 //       //   }
            
 //    });

    //$('div.setup-panel div a.btn-indigo').trigger('click');
});
</script>


          <div class="col-md-9">
               <div class="panel panel-default">
              <!--   <div class="panel-heading">
                  <h3 class="panel-title">Listing Bikes</h3>
                </div> -->
                <div class="panel-body">
                  <!-- Steps form -->
<div class="card">
   <h2> <a href="approve_bikes.php" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Previous</a>
Bike Profile</h2>
   <hr>
       <?php   
                 $data =  get_bike_per_id($_GET['id']);
                 $idd = $_GET['id'];
               foreach((array) $data as $datas) {
                 $daily = $datas['daily'];
                 $weekly = $datas['weekly'];
                 $monthly = $datas['monthly'];
                 $Insure_details = $datas['Insure_details'];
                 $insurance_check1 = $datas['insurance_check1'];
                 $insurance_check2 = $datas['insurance_check2'];
                 $insurance_check3 = $datas['insurance_check3'];
                 $terms = $datas['terms'];  

       ?>
               <form role="form"  id="bike_profile_updates" method="post"  enctype="multipart/form-data" >
           <input type="hidden" name="opera" value="update_bike_profile">
          <input type="hidden" name="idd" value="<?php echo $idd;  ?>">
           
                 <fieldset id="rates_information" class="">
            <div class="row setup-content" id="step-11">
                <div class="col-md-12">
                
                    <div class="row" >
                <div class="col-md-12">                    
                  <div>
                        <div class="col-md-6" style="margin-top: 10px">
                        <h4 class="font-weight-bold pl-0 my-4" style="margin:auto"><strong>Your Rates (DKK)</strong></h4>
                        <div id="address">
                    <div class="form-group md-form" style="margin-top: 10px">
                        <label for="companyName" data-error="wrong" data-success="right">1 day Price</label>
                        <input  type="text" required="required"  class="form-control validate" id="daily" name="daily" placeholder="Enter daily amount" value="<?php  echo $daily; ?>" >
                    </div>
                     
                    <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">7 days Price</label>
                        <input placeholder="Enter weekly amount" name="weekly" id="weekly" required="required"  onkeyup="copyTo(this)" type="text"  class="form-control validate" value="<?php  echo $weekly; ?>" >
                        <label  id="weeklyy"></label>
                    </div>

                    <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">30 days Price</label>
                        <input id="monthly" type="text"  name="monthly" required="required"  onkeyup="copyTomonthly(this)"  class="form-control validate" placeholder="Enter monthly amount" value="<?php  echo $monthly; ?>" >
                         <label id="monthlyy"></label>
                    </div>
                     
 
                </div>
                    </div>
                  <div class="col-md-6" style="margin-top: 10px">
                     <h4 class="font-weight-bold pl-0 my-4" style="margin:auto"><strong>Insurance</strong></h4>
                    <div class="form-group md-form" style="margin-top: 10px">
                        <div>
                       <div class="form-check"  style="margin-bottom: 5px">
                        <?php  if ($insurance_check1 != "") { ?>
                          <input type="checkbox" class="filled-in form-check-input validate" id="insurance_check1" name="insurance_check1" value="Third Party Insurance" checked>
                            <label class="form-check-label " for="checkbox101">Third Party Insurance</label>

                       <?php  } else { ?>
                             <input type="checkbox" class="filled-in form-check-input validate" id="insurance_check1" name="insurance_check1" value="Third Party Insurance" >
                            <label class="form-check-label " for="checkbox101">Third Party Insurance</label>
                     <?php  }  ?>

                      
                            
                        </div>
                        <div class="form-check" style="margin-bottom: 5px">
                            <?php  if ($insurance_check2 != "") { ?>
                            <input type="checkbox" class="filled-in form-check-input" id="insurance_check2" name="insurance_check2" value="Personal Insurance" checked="">
                            <label class="form-check-label" for="checkbox101">Personal Insurance</label>

                       <?php  } else { ?>
                               <input type="checkbox" class="filled-in form-check-input" id="insurance_check2" name="insurance_check2" value="Personal Insurance">
                            <label class="form-check-label" for="checkbox101">Personal Insurance</label>
                     <?php  }  ?>


                         
                        </div>
                               
                         <div class="form-check" style="margin-bottom: 5px">

                            <?php  if ($insurance_check2 != "") { ?>
                             <input type="checkbox" class="filled-in form-check-input" id="insurance_check3" name="insurance_check3" value="Extra Insurance" checked>
                            <label class="form-check-label" for="checkbox101">Extra Insurance</label>

                       <?php  } else { ?>
                               <input type="checkbox" class="filled-in form-check-input" id="insurance_check3" name="insurance_check3" value="Extra Insurance">
                            <label class="form-check-label" for="checkbox101">Extra Insurance</label>
                     <?php  }  ?>

                         
                        </div>                               
                           

                    </div>
                    <div style="margin-top: 10px">
                        <label for="companyAddress" data-error="wrong" data-success="right">Insurance Details</label>
                        <textarea class="form-control validate" required="required"  id="Insure_details" name="Insure_details" rows="3" style="height: 100px" placeholder="Insurance Details"><?php  echo $Insure_details; ?></textarea>
                    </div>
                               
                    </div>
                    
                    </div>
                      <div class="col-md-12" style="margin-top: 10px">
                        <div class="form-group md-form mt-3">
                        <label for="companyAddress" data-error="wrong" data-success="right">Terms and conditions</label>
                         <textarea class="form-control validate" required="required"  id="terms" name="terms" rows="3" style="height: 100px" placeholder="Here you can write terms which rentee must accept and follow"><?php  echo $terms; ?></textarea>
                    </div>
                     
                   


                      </div>
                   

                    
            </div>
            </div>
            </div>


                
                    <button class="btn btn-default btn-rounded float-right" type="button"  id="bike_pro_edit">Save Changes </button>
                </div>
            </div>
        </fieldset>
        <?php     }  ?>
      </form>
                </div>
                    </div>
                        </div>
            </div>


          <!-- The Modal -->



 <?php include('footer.php') ; ?>



<!--  <script src="https://code.jquery.com/jquery-1.12.4.js/loadingoverlay.min.js"></script>  -->
 <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="js/view_bikes.js"></script> 
<!-- The main application script -->
<script type="text/javascript">
  $('.dropdown-toggle').click(function (){
            dropDownFixPosition($('button'),$('.dropdown-menu'));
        });
function dropDownFixPosition(button,dropdown){
      var dropDownTop = button.offset().top + button.outerHeight();
        dropdown.css('top', dropDownTop + "px");
        dropdown.css('left', button.offset().left + "px");
}
</script>
           
<script type="text/javascript">
      
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
     function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag1').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
     function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
    $("#profile1-img").change(function(){
        readURL1(this);
    });
     $("#profile2-img").change(function(){
        readURL2(this);
    });
</script>
<script type="text/javascript">
       
$("#edit_details").click(function(){
      var form = $("#add_Bike");

      if (form.valid() == true){
            //  current_fs = $('#personal_information');
            // next_fs = $('#rates_information');
           // Show full page LoadingOverlay
                $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 2000);
            // current_fs.hide();
           
         var formData = new FormData($('#addBike')[0]);
        var ajaxRequest = $.ajax({
        type: "POST",
        //url: '../includes/manage_post',
       url: '../includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 3000);
        console.log(xhr);
        if(xhr==1) {
          alert('Bike profile has been updated')
          // $('#successBtn').modal('toggle');  
          //  next_fs = $('#rates_information');
          //  current_fs = $('#account_information');
          //   next_fs.hide();
            // window.location.href = "listing_bikes.php";
             window.location.href = "approve_bikes.php";
         // $('#addBike')[0].reset();
          //current_fs.show();  
          
           

           // document.getElementById('resQuotes').style.display = "none";
           // document.getElementById('res').style.display = "none";
           // document.getElementById('resShow').style.display = "block";
           
          //toastr.success("Email has been sent succesffully.", "Status");
           //$('#add_insurance')[0].reset();
        }else {
           // toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });
          //  next_fs.show(); 
</script>
                           