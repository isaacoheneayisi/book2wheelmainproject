<?php
/*
 * For more details
 * please check official documentation of DataTables  https://datatables.net/manual/server-side
 * Coded by charaf JRA
 * RefreshMyMind.com
 */

include_once ('../../includes/config.php');
//$conn = pg_connect(LOCAL_CONN );
$conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE) or die('Could not connect to db Server'.mysql_error());
//ini_set('session.cookie_domain', 'myaidfund.com/admin');
session_start();

get_all_data();

function get_all_data() {
	global $conn;
	$sql = "SELECT id,file,model,make,year_bike,transM,displacement,PlateNumber FROM bikes WHERE active=0 ORDER BY id DESC";

	$result = $conn->query($sql);
	$output = '
<table id="view_campaign" class="table table-striped table-bordered table-hover" >
                      <thead>
                                    <tr><th> ID</th>
                                        <th> Action</th>
                                        <th> images</th>
                                        <th> model</th>
                                        <th> make</th>
                                        <th> year_bike </th>
                                        <th> transM</th>
                                        <th> displacement</th>                                     
                                        <th> PlateNumber</th>
                                        <th> Action</th>
                                   </tr>
                                </thead>
                     <tbody>
    ';
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_object()) {

			$output .= "
                <tr>
                                 <td>$row->id</td>
			                    <td> <img class='btn btn-default dropdown-toggle' data-toggle='dropdown' src='../user_profile/img/setting.png' width='60px' height='50px' />
								      <ul class='dropdown-menu' role='menu'>
								          <li><a href='view_details.php?id=$row->id'>Bike Profile</a></li>
								          <li><a href='pricing.php?id=$row->id'>Pricing</a></li>
								          <li><a href='location.php?id=$row->id'>Location</a></li>								      
								          <li class='divider'></li>
								          <li><a href='../view_bike_preview.php?id=$row->id'>view bike</a></li>
      								  </ul></td>
                    <td><img src='../user_profile/bikes/$row->file' width='50px' height='50px'/></td>
                    <td>$row->model</td>
                    <td>$row->make</td>
                    <td>$row->year_bike</td>
                    <td>$row->transM</td>
                    <td>$row->displacement</td>
                    <td>$row->PlateNumber</td>    
                      <td>
                      <a href='appdis.php?id=$row->id' class='btn btn-primary btn-lg active' role='button' aria-pressed='true'>Approve Bike</a>                   

                </tr>
";
		}
	} else {
		$output .= '
                <tr>
                     <td colspan="5">No Data Found</td>
                </tr>
           ';
	}

	$output .= '</tbody></table>';
	echo $output;

}

die();
/* IF Query comes from DataTables do the following */
if (!empty($_POST)) {
	/*
	 * Database Configuration and Connection using mysqli
	define("MyTable", "smslog");
	 */
	define("MyTable", "mfund");
	// define("MyTable", "kb_living");
	// define("MyTable", "mfund");
	/* END DB Config and connection */

	/*
	 * @param (string) SQL Query
	 * @return multidim array containing data array(array('column1'=>value2,'column2'=>value2...))
	 *
	 */

	/* Useful $_POST Variables coming from the plugin */
	$draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
	/* $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
	$orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
	$orderType = $_POST['order'][0]['dir']; // ASC or DESC
	$start  = $_POST["start"];//Paging first record indicator.
	$length = $_POST['length'];*/
	//Number of records that the table can display in the current draw
	/* END of POST variables */

	function getData() {
		global $conn;

		$sql = "SELECT id,ownerid,country,title,date_created,date_end,amount_goal,amount_achieved,publish_status FROM campaign  ORDER BY id DESC";

		$result = $conn->query($sql);
		$data   = array();
		$res    = $result->fetch_object();
		foreach ($res as $row) {
			$data[] = $row;
		}

		return $data;
	}

	function getData2() {
		$orderByColumnIndex = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
		$orderBy            = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
		$orderType          = $_POST['order'][0]['dir'];// ASC or DESC
		$start              = $_POST["start"];//Paging first record indicator.
		$length             = $_POST['length'];
		$key                = $_POST['search']['value'];

		global $conn;
		$sql    = "SELECT * FROM campaign WHERE publish_status ilike '%$key%' ORDER BY $orderBy $orderType LIMIT $length OFFSET $start";
		$result = $conn->query($sql);
		$data   = array();
		$res    = $result->fetch_object();
		foreach ($res as $row) {
			$data[] = $row;
		}
		return $data;
	}

	$recordsTotal = count(getData());

	/* SEARCH CASE : Filtered data */
	if (!empty($_POST['search']['value']) || !empty($_POST['order'][0]['column'])) {

		/* WHERE Clause for searching */
		$data            = getData2();
		$recordsFiltered = $recordsTotal;
		/* END SEARCH */
	} else {
		//$sql = sprintf("SELECT * FROM %s ORDER BY %s %s limit %d , %d ", MyTable ,$orderBy,$orderType ,$start , $length);
		$data            = getData();
		$recordsFiltered = $recordsTotal;
	}

	/* Response to client before JSON encoding */
	$response = array(
		"draw"            => intval($draw),
		"recordsTotal"    => $recordsTotal,
		"recordsFiltered" => $recordsFiltered,
		"data"            => $data,
	);

	echo json_encode($response);

} else {
	echo "NO POST Query from DataTable";
}
?>