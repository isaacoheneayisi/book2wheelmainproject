

<?php include('header.php') ; 
 $page_title ="list Bike";
?>

      <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/pepper-grinder/jquery-ui.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css">
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<style type="text/css">
  .products {
  display: flex;
  flex-wrap: wrap;
}
.product-card {
  padding: 2%;
  flex-grow: 1;
  flex-basis: 16%;

  display: flex; /* so child elements can use flexbox stuff too! */
}
.product-image img {
  max-width: 100%;
  width: 1000px;
  height: 300px;
}

#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
.ui-datepicker {
  width: 20em;
  height: 15em;
  }
</style>



          <div class="col-md-9">
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Listing Bikes</h3>
                </div>
                <div class="panel-body">
                  <!-- Steps form -->
<div class="card">
   <h2> Manage Bike Dates </h2>
   <hr>
    <div class="card-body mb-4">

   
          <?php 
                   $id= $_GET['id'];
                  $m_bike  =get_bike_by_id($id);
                  //echo $data;
                  

           ?>
      <div class="row" style="padding-right: 10px; padding-left: 10px">
           <div class="col-md-12">
         <?php   foreach((array)$m_bike as $bike_arr ) {
                  $image ="bikes/".$bike_arr['file'];
                  //echo $image;
                  $id = $bike_arr['id'];
                  $make = $bike_arr['make'];
                  $model = $bike_arr['model'];
                  $displacement= $bike_arr['displacement'];
                  $transM= $bike_arr['transM'];
                  $daily= $bike_arr['daily'];
                  ?>
         <div class="col-md-9">
       
          <table>

              <td>
                <div class="product-image">
                  <h5> <?php  echo $make; ?> </h5>

         <?php  
                
         echo '<img  id="myImg" class="img-thumbnail  modal-content" src="' . $image . '"/>';  ?>

   
             </div>
           </td>
         </table>
       </div>
        <div class="col-md-3">
              <div class="product-info">
       <h5>Make : <?php  echo "<b>".$make."</b"; ?></h5>
      <h5> Model :<?php  echo $model."</b"; ?></h5>
     <h5>Dispalcement : <?php  echo "<b>". $displacement."</b"; ?></h5>
       <h5>Transmission : <?php  echo "<b>".$transM."</b"; ?></h5>
        <h5>1 day Price = <?php  echo "<b>".$daily."</b"; ?></h5> <br />
          <button type="button" class="btn btn-danger">Edit Bike Details</button>

    </div>
    </div>

  </div>
     <?php } ?>   
</div>

 <div class="row" >
 <div class="col-md-12">
   
                <h2> Manage Bike Dates </h2>
                <form role="form"  id="m_dates" method="post"  >                
   <hr>           <input type="hidden" name="opera" value="m_datees">
                    <input type="hidden" name="bike_id" id="bike_id" value="<?php  echo $id; ?>">
                   <div class="col-md-2">
                <div id="mdp-demo"></div>
                <br /><br /> 
                <input type="submit" value="save dates">

              </div>
              
               
               </form>
             
                <?php
                  $data = get_dates($id);
                // $temp = array("abc","xyz");
 
              
                                 
                              if ($data ==""){                                 
                                  ?>
                                   <input type="hidden" id="date_strings_dis" name="date_strings_dis" value="<?php // echo  "" ;?>"> </div>
                                   <?php

                              } else{
                                  $result_string = "'" . str_replace(",", "','", $data) . "'";
                                 ?>
                                  <input type="hidden" id="date_strings_dis" name="date_strings_dis" value="<?php echo  $result_string ;   ?>">
  
                               
                           <?php   }
                                  ?>
         
          </div>    
          </div> 
    <!--  </div>
       <div id="caption"></div>
    </div> -->

<!-- Steps form -->  

          <!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
 

 <?php include('footer.php') ; ?>

<script type="text/javascript" src="js/jquery-ui.multidatespicker.js"></script>

      <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
      <script type="text/javascript" src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js"></script>
<script type="text/javascript">
   var dates_strings  = document.getElementById('date_strings_dis').value;
    if (dates_strings == ""){
     
      $('#mdp-demo').multiDatesPicker({   
    minDate: 0,
    addDisabledDates: false
  });
       
    } else {
   var string=[];
   var string = dates_strings.split(",");
   var date = [];
    for (var i = 0; i < string.length; i++) {
          date.push(new Date(string[i].toString()).toLocaleDateString('en-US'));

    }
   // alert(date);
   // var dataa= string;
    ////var date = new Date(string);
    //alert(string[0]);
  
   
  for (var i = 0; i < date.length; i++) {

  $('#mdp-demo').multiDatesPicker({   
    minDate: 0,
    addDisabledDates: [date[i]]
  });
}
}
//    $("#mdp-demo").click(function(){
    
    
// });
     $('#m_dates').submit(function(e){
e.preventDefault();
console.log($("#m_dates").valid());
if($("#m_dates").valid()){
$("#m_dates").val();
  // //how full page LoadingOverlay
                $.LoadingOverlay("show");

                // Hide it after 3 seconds
                setTimeout(function(){
                    $.LoadingOverlay("hide");
                }, 3000);
 var dates = $('#mdp-demo').multiDatesPicker('getDates');
    var  id = document.getElementById('bike_id').value ;  
    var formData = new FormData($('#m_dates')[0]);
    var ajaxRequest = $.ajax({
        type: "GET",
        //url: '../includes/manage_post.php',
        url: '../includes/manage_post.php',
        contentType: false,
        processData: false,
        data: "bike_id="+ id + "&date_strings=" + dates+"&opera=m_datees",
        dataType: 'json'

    });
    ajaxRequest.done(function (data) {
   alert("dates saved");
   //  window.location.href = "manage_bike.php";
    // window.location.href = "bikesearch.php";
        //$.unblockUI();
       // console.log(xhr);

    });

  }
 });                
 
</script>
<!-- The main application script -->

                        
        