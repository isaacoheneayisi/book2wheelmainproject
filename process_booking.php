<?php
ob_start();  
 include('search_header.php'); 
 include('includes/allFunctions.php'); 
  session_start();
  
 //include('includes/config.php');
  $page_title ="bikedetails"; 

   //Set useful variables for paypal form
  // $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypalURL = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
//$paypalID = 'ayiscoo@yahoo.com'; //Business Email
$paypalID = 'fur@book2wheel.com'; //Business Email   
//$paypalID = 'fur-facilitator@book2wheel.com'; //Business Email
if (empty($_SESSION['total'])){
  header('Location: index.php',true,301);
  exit;
  }

 ?>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<hr>
<link rel="stylesheet" href="css/payment.css" />

<div class="container" style="margin-top: 70px; background-color: #EDF0F2">
  <main class="page payment-page">
    <section class="payment-form dark">
      <div class="container">
        <div class="block-heading">
          <h2>Payment</h2>
          <p>You will pay 20% of the total priced proposed by the owner and pay the rest to him on pickup</p>
        </div>
        <form action="<?php echo $paypalURL; ?>" method="post">
          <div class="products">
            <h3 class="title">Your Booking</h3>
            <div class="item">
              <span class="price"><?php  
                     if ($_SESSION['num_days'] ==0){
                      echo 1;
                     }else {
                      echo $_SESSION['num_days'];
                     }
              ?></span>
              <p class="item-name">Number of Days</p>
             <!--  <p class="item-description">Lorem ipsum dolor sit amet</p> -->
            </div>
            
            <div class="item">
              <span class="price"><?php echo $_SESSION['total'];  ?></span>
              <p class="item-name">Price for Days Chosen</p>
           <!--  <p class="item-description">i: </p>  -->
              
            </div>
            <div class="item">
              <span class="price"> From : <?php echo $_SESSION['begin_book_date'] ." "."to"." ".$_SESSION['end_book_date']  ?> </span>              
              <p class="item-name">Choosen Dates</p>
              
           <!--  <p class="item-description">i: </p>  -->
              
            </div>
            <div class="item">
              <span class="price"><img src="user_profile/bikes/<?php echo $_SESSION['image_url'];  ?>" width="200px" height="70px"></span>
              <p class="item-name">Chosen Bike</p>
             <!--  <p class="item-description">Lorem ipsum dolor sit amet</p> -->
            </div><br /> <br />

             <div class="item">
              <span class="price"><?php echo $_SESSION['total'];  ?></span>
              <p class="item-name">Price for <?php echo $_SESSION['num_days'] ?> </p>
           <!--  <p class="item-description">i: </p>  -->
              
            </div>
            <br />
            <?php
                $twenty_per = (20/100)* $_SESSION['total'];
                $_SESSION['total_per'] = $twenty_per ;
            ?>
            <div class="total">20% of the Total<span class="price">PHP  <?php echo $twenty_per;  ?></span></div>
          </div>

          <!-- Identify your business so that you can collect the payments. -->
        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">
        
        <!-- Specify a Buy Now button. -->
       <input type="hidden" name="cmd" value="_xclick">
        
        <!-- Specify details about the item that buyers will purchase. -->
        <input type="hidden" name="item_name" value="MotoBike Booking">
        <input type="hidden" name="item_number" value="<?php echo $_SESSION['num_days']; ?>">
        <input type="hidden" name="amount" value="<?php echo $twenty_per;  ?>">
        <input type="hidden" name="currency_code" value="PHP">
        <input type='hidden' name='notify_url' value='https://book2wheel.com/asia/pay_success.php'>
        <!-- Specify URLs -->
  <!--       <input type='hidden' name='cancel_return' value='http://localhost/book2wheel/pay_cancel.php'>
        <input type='hidden' name='return' value='http://localhost/book2wheel/pay_success.php'> -->
         <input type='hidden' name='cancel_return' value='https://book2wheel.com/asia/pay_cancel.php'>
         <input type='hidden' name='return' value='https://book2wheel.com/asia/pay_success.php'> 
         <input type="hidden" name="notify_url" value="https://book2wheel.com/asia/ipn.php" />

        <!-- <input type='hidden' name='notify_url' value='http://www.codexworld.com/ipn.php'> -->
        <!-- Display the payment button. -->
       <!--  <input type="image" name="submit" border="0"
        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">
        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > -->
        <!--   <div class="card-details">
            <h3 class="title">Credit Card Details</h3>
            <div class="row">
              <div class="form-group col-sm-7">
                <label for="card-holder">Card Holder</label>
                <input id="card-holder" type="text" class="form-control" placeholder="Card Holder" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>
              <div class="form-group col-sm-5">
                <label for="">Expiration Date</label>
                <div class="input-group expiration-date">
                  <input type="text" class="form-control" placeholder="MM" aria-label="MM" aria-describedby="basic-addon1">
                  <span class="date-separator">/</span>
                  <input type="text" class="form-control" placeholder="YY" aria-label="YY" aria-describedby="basic-addon1">
                </div>
              </div>
              <div class="form-group col-sm-8">
                <label for="card-number">Card Number</label>
                <input id="card-number" type="text" class="form-control" placeholder="Card Number" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>
              <div class="form-group col-sm-4">
                <label for="cvc">CVC</label>
                <input id="cvc" type="text" class="form-control" placeholder="CVC" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>
              <div class="form-group col-sm-12">
                <button type="button" class="btn btn-primary btn-block">Proceed</button>
              </div>
            </div>
          </div> -->
           <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-primary btn-block"  value="Proceed with Paypal" width="126px" height="33px"></a></button><br />            
                       <p><input type="radio" name='visa' id="visa" value='P' id="visa"/> Pay with Visa<img width="50px" height="50px" src="img/visa.jpg" ><img width="50px" height="50px" src="img/mastercard.jpg"> </p> 

              </div>
  </form>
    <div id="pay_master">
      <form role="form"  id="visa_payment" method="post"   >
       <input type="hidden" name="opera" value="visa_master">
          <div class="products">
            <p><input type="radio" name='paypal' value='paypal' id="paypal"/> Hide </p> 
                <div class="card-details">
            <h3 class="title">Billing  Information <a href="#"   width="126px" height="33px"><img src="img/visa.jpg" >
                        <img src="img/mastercard.jpg"> </a>
</h3>
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="card-holder">Billing Adsress</label>
                <input id="card-holder" type="text" class="form-control" id="billing_address" name="billing_address" placeholder="Billing Adsress" aria-label="billing address" aria-describedby="basic-addon1">
              </div>
               

               <div class="form-group col-sm-12">
                <label for="card-holder">Billing City</label>
                <input id="card-holder" type="text" d="billing_city" name="billing_city"   class="form-control" placeholder="Billing City" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>
               
               <div class="form-group col-sm-12">
                <label for="card-holder">Billing Country</label>
                <input id="card-holder" type="text" id="billing_country" name="billing_country" class="form-control" placeholder="Billing Country" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>
               <div class="form-group col-sm-12">
                <label for="card-holder">Billing  Postal Code</label>
                <input id="card-holder" type="text" id="billing_postcode" name="billing_postcode" class="form-control" placeholder="Postal Code" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>            
             
              <div class="form-group col-sm-12">
                <label for="card-number">Email</label>
                <input id="card-number" type="email" id="email" name="email" class="form-control" placeholder="Email" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>

              <?php
                    $euro_charge = convertCurrency($twenty_per, "PHP", "EUR");
              ?>
                <div class="form-group col-sm-12">
                <label for="card-number" style="font-size: 20px">PHP <?php echo  $twenty_per ?> = <?php echo  $euro_charge ?> EUR </label>
               <input id="card-number" type="hidden" id="amount" name="amount" class="form-control" value="<?php echo $euro_charge;  ?>" aria-label="Card Holder" aria-describedby="basic-addon1">
              </div>
              <div class="form-group col-sm-12">
                <input type="submit"  class="btn btn-primary btn-block" value="Proceed   EUR <?php echo $euro_charge;  ?>" />
              </div>
            </div>
          </div>
   
      </div>
    </form>
   </div>
      </div>
    </section>
  </main>


</div>


<!-- <script src="https://www.paypalobjects.com/api/checkout.js"></script> -->
<!-- <script>
paypal.Button.render({
  env: 'sandbox',
  client: {
    sandbox: 'demo_sandbox_client_id'
  },
  payment: function (data, actions) {
    return actions.payment.create({
      transactions: [{
        amount: {
          total: '0.01',
          currency: 'USD'
        }
      }]
    });
  },
  onAuthorize: function (data, actions) {
    return actions.payment.execute()
      .then(function () {
        window.alert('Thank you for your purchase!');
      });
  }
}, '#paypal-button');
</script> -->


<?php
function convertCurrency($amount, $from, $to){
  $conv_id = "{$from}_{$to}";
  $string = file_get_contents("https://free.currencyconverterapi.com/api/v3/convert?q=$conv_id&compact=ultra");
  $json_a = json_decode($string, true);
 
  return $amount * round($json_a[$conv_id], 4);
}


?> 

<?php  include ('search_footer.php'); ?>
 <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
  $('#pay_master').hide();
</script>
<script type="text/javascript">
  $(function() {
    $('input[type="radio"]').change(function() {
        var rad = $(this);
        //('input[type="radio"]').addClass('none');
        if (rad.is(':checked'))
          
         $('#pay_master').show();
       $("#visa").attr("checked", false);
       $("#paypal").attr("checked", false);
    });

    $('#paypal').change(function() {
        var rad = $(this);
        //('input[type="radio"]').addClass('none');
        if (rad.is(':checked'))
           $("#visa").attr("checked", false);
        $("#paypal").attr("checked", false);
         $('#pay_master').hide();
      
    });
});
</script>